<?php $this->load->view($this->theme . '/admin/partials/header.php');?>

<div class="container">
    <?php $this->load->view($this->theme . '/admin/partials/logo.php');?>
    
    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view($this->theme . '/admin/partials/sidebar.php');?>
        </div>
        <div class="col-md-9">
            <div class="row">    
                <div class="col-md-2">
                    <span class="fa fa-graduation-cap icon-header"></span>
                </div>
                <div class="col-md-10">
                    <h4>Daftar Kelompok Soal</h4>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="#">Beranda</a>&nbsp;&nbsp;<span class="fa fa-angle-right"></span></li>
                            <li><a href="#">Pengaturan Soal</a>&nbsp;&nbsp;<span class="fa fa-angle-right"></span></li>
                            <li>Daftar Kelompok Soal</li>
                        </ul>
                    </div>
                </div>
            </div>

            <hr />

            <div class="row">
                <div class="col-md-6">
                    <h4>Kelompok Soal</h4>
                </div>    
                <div class="col-md-6">
                    <a href="#" class="btn btn-info btn-add-group pull-right"><span class="fa fa-plus"></span> Tambah</a>
                </div>    
            </div>
            
            <div class="box margin-md-top">
                
                <form class="form-inline margin-md-bottom" method="get" action="<?php echo current_url();?>">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group pull-left">
                                <input type="number" name="sort" class="form-control" value="10" placeholder="Sort" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group pull-right">
                                <div class="input-group">
                                    <input type="text" name="keyword" class="form-control" value="<?php echo $this->input->get('keyword')?>" placeholder="Search keyword .."/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">Search</button>
                                    </span>
                                </div><!-- /input-group -->
                            </div>
                        </div>
                    </div>
                </form>

                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Judul Kelompok Soal</th>
                        <th>Durasi Pengerjaan</th>
                        <th>Soal Terhubung</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    
                    <?php 
                    $num = 1; 
                    foreach($groups as $group) 
                    {
                        $connectedQuestions = $this->Question_model->getTotalQuestions($group->id, null, 'all');
                        ?>
                        <tr>
                            <td><?php echo $num;?></td>
                            <td><?php echo $group->title;?></td>
                            <td><?php echo $group->duration;?> menit</td>
                            <td><?php echo $connectedQuestions;?></td>
                            <td>
                                <?php if ($group->status == 'draft') :?>
                                    <span class="label label-danger"><?php echo $group->status;?></span>
                                <?php else:?>
                                    <span class="label label-success"><?php echo $group->status;?></span>
                                <?php endif;?>
                            </td>
                            <td>
                                <a href="#" class="btn btn-warning btn-share btn-xs" data-group-id="<?php echo $group->id?>" data-title="<?php echo $group->title?>"><span class="fa fa-share"></span>&nbsp;&nbsp;Share To</a>
                                <a href="#" class="btn btn-info btn-edit-group btn-xs" data-id="<?php echo $group->id?>"><span class="fa fa-pencil"></span>&nbsp;&nbsp;Edit</a>
                                <a href="<?php echo base_url('admin/group/remove/' . $group->id)?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span>&nbsp;&nbsp;Hapus</a>
                            </td>
                        </tr>
                        <?php
                        $num++; 
                    }
                    ?>
                </table>

                <div class="pagination">
                    <?php echo $links;?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="groupModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah/Edit Kelompok Soal</h4>
            </div>
            <div class="modal-body">
                <div class="message"></div>
                
                <input type="hidden" id="id" />
                <input type="hidden" id="label" value="quiz"/>
                
                <div class="form-group">
                    <label for="">Judul Kelompok Soal</label>
                    <input type="text" id="title" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="">Sub-Judul</label>
                    <input type="text" id="subtitle" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="">Durasi (Dalam Menit)</label>
                    <input type="number" id="duration" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="">Status</label>
                    <select id="status" class="form-control">
                        <option value="publish" selected>Publish</option>
                        <option value="draft">Draft</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary btn-save">Simpan</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="shareModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title title"></h4>
            </div>
            <div class="modal-body">
                <div class="message"></div>
                
                <div class="form-group">
                    <label for="">Sekolah Pengakses :</label>
                    <div class="connected_school">-</div>
                </div>
                
                <div class="form-group">
                    <label for="">Pilih Sekolah</label>
                    <select id="school_id" class="form-control">
                        <label>Sekolah</label>
                        <option value="#" selected>Pilih ..</option>
                        <?php foreach($schools as $school) :?>
                            <option value="<?php echo $school->id?>"><?php echo $school->name?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary btn-add-school">Tambahkan</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
var base_url = $('#base_url').val();
var group_id;
var action = 'insert';

$('.btn-share').click(function(){
    group_id = $(this).attr('data-group-id');
    var title = $(this).attr('data-title');

    $('.title').html('Kelompok Soal <b>' + title + '</b>');
    
    getConnectedSchool(group_id);

    $('#shareModal').modal('show');

    return false;
});

$('.btn-add-school').click(function(){
    var school_id = $('#school_id').val();

    $.post( base_url + 'admin/group/share', { school_id: school_id, group_id: group_id })
    .done(function( data ) {
        
        if (data.status == 'failed') {
            $('.message').show().html('<div class="alert alert-danger">'+ data.message +'</div>');
        } else {
            getConnectedSchool(group_id);
            $('.message').show().html('<div class="alert alert-success">'+ data.message +'</div>');
        }
    });
});

$( document ).on( 'click', '.btn-remove-school', function() {
    var school_id = $(this).attr('data-school-id');
    
    $.post( base_url + 'admin/group/removeConnected', { school_id: school_id, group_id: group_id })
    .done(function( data ) {
        
        if (data.status == 'failed') {
            $('.message').show().html('<div class="alert alert-danger">'+ data.message +'</div>');
        } else {
            getConnectedSchool(group_id);
            $('.message').show().html('<div class="alert alert-success">'+ data.message +'</div>');
        }
    });

    return false;
});

$('.btn-add-group').click(function(){
    action = 'insert';

    $('#groupModal').modal('show');

    return false;
});

$('.btn-edit-group').click(function(){
    action = 'update';

    $('.message').show().html('');

    var id = $(this).attr('data-id');

    $.getJSON( base_url + 'admin/group/detail/' + id, function( data ) {
        
        $('#id').val(data.id);
        $('#title').val(data.title);
        $('#subtitle').val(data.subtitle);
        $('#label').val(data.label);
        $('#duration').val(data.duration);
        $('#status').val(data.status);
        
        $('#groupModal').modal('show');        
    });

    return false;
});

$('.btn-save').click(function(){
    var id = $('#id').val();
    var title = $('#title').val();
    var subtitle = $('#subtitle').val();
    var label = $('#label').val();
    var duration = $('#duration').val();
    var status = $('#status').val();

    $('.btn-save').html('Loading ..');
    
    $.post( base_url + 'admin/group/' + action, { id:id, label:label, title: title, subtitle: subtitle, duration: duration, status: status })
    .done(function( data ) {
        
        $('.btn-save').html('Simpan');
        
        if (data.status == 'failed') {
            $('.message').show().html('<div class="alert alert-danger">'+ data.message +'</div>');
        } else {
            alert(data.message);
            location.reload();
        }
    });
});

function getConnectedSchool()
{
    $('.connected_school').html('');

    $('.connected_school').append('<ul>');

    $.getJSON( base_url + 'admin/group/getConnected/' + group_id, function( data ) {
        $.each( data, function( key, value ) {
            $('.connected_school').append('<li>' + value.name + ' <a href="#" class="btn-remove-school" data-school-id="'+ value.school_id +'"><span class="fa fa-trash"></span></a></li>');
        });
    });

    $('.connected_school').append('</ul>');
    
    return false;
}
</script>

<?php $this->load->view($this->theme . '/admin/partials/footer.php');?>