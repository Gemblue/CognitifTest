<?php $this->load->view($this->theme . '/admin/partials/header.php');?>

<div class="container">
    <?php $this->load->view($this->theme . '/admin/partials/logo.php');?>
    
    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view($this->theme . '/admin/partials/sidebar.php');?>
        </div>
        <div class="col-md-9">
            <div class="row">    
                <div class="col-md-2">
                    <span class="fa fa-graduation-cap icon-header"></span>
                </div>
                <div class="col-md-10">
                    <h4>Laporan Angket</h4>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="#">Beranda</a>&nbsp;&nbsp;<span class="fa fa-angle-right"></span></li>
                            <li>Laporan Angket</li>
                        </ul>
                    </div>
                </div>
            </div>

            <hr />

            <h4>Pencarian</h4>

            <div class="box">
                <form action="<?php echo current_url();?>" method="get" class="form-inline">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" value="<?php echo $this->input->get('name')?>" placeholder="Nama Siswa"/>
                    </div>
                    <div class="form-group">
                        <input type="text" name="nisn" class="form-control" value="<?php echo $this->input->get('nisn')?>" placeholder="NISN"/>
                    </div>
                    <div class="form-group">
                        <select name="school_id" class="form-control">
                            <option value="" selected>Sekolah ..</option>
                            <?php foreach($schools as $school) :?>
                                <option value="<?php echo $school->id?>" <?php echo ($school->id == $this->input->get('school_id')) ? 'selected' : ''; ?>><?php echo $school->name?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="group_id" class="form-control">
                            <option value="" selected>Angket ..</option>
                            <?php foreach($groups as $group) :?>
                                <option value="<?php echo $group->id;?>" <?php echo ($group->id == $this->input->get('group_id')) ? 'selected' : ''; ?>><?php echo $group->title?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success">Cari</button>
                </form>
            </div>

            <h4>Hasil Angket</h4>

            <div class="box">
                <div class="row">
                    <div class="col-md-6">
                        <form action="form-inline">
                            <div class="form-group pull-left">
                                <input type="number" class="form-control" value="10" placeholder="Sort" />
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6"></div>
                </div>

                <?php if (!empty($results)) :?>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Angket</th>
                            <th>Nama Siswa</th>
                            <th>Sekolah</th>
                            <th>NISN</th>
                            <th>Tanggal Pengisian</th>
                            <th></th>
                        </tr>
                        
                        <?php $num = 1; foreach($results as $result) :?>
                            <tr>
                                <td><?php echo $num;?></td>
                                <td><?php echo $result->title;?></td>
                                <td><?php echo $result->name;?></td>
                                <td><?php echo $result->school_name;?></td>
                                <td><?php echo $result->nisn;?></td>
                                <td><?php echo $result->created_at;?></td>
                                <td>
                                    <a href="<?php echo site_url('admin/SurveyResult/detail/' . $result->result_id)?>" class="btn btn-info btn-xs"><span class="fa fa-search"></span>&nbsp;&nbsp;Jawaban</a>
                                </td>
                            </tr>
                        <?php $num++; endforeach;?>
                    </table>
                    
                    <a href="<?php echo site_url('admin/SurveyResult/export?name=' . $this->input->get('name') . '&nisn=' . $this->input->get('nisn') . '&school_id=' . $this->input->get('school_id') . '&group_id=' . $this->input->get('group_id'))?>" class="pull-right"><span class="fa fa-download"></span>&nbsp;&nbsp;Download</a>
                    
                    <div class="pagination">
                        <?php echo $links;?>
                    </div>
                <?php else:?>
                    Data tidak ditemukan ..
                <?php endif;?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view($this->theme . '/admin/partials/footer.php');?>