<section class="menu">
    <h4>Insight</h4>
    <ul class="list-group">
        <li class="list-group-item"><span class="fa fa-area-chart fa-fw"></span>&nbsp;&nbsp;<a href="<?php echo site_url('admin/dashboard')?>">Statistik</a></li>
        <li class="list-group-item"><span class="fa fa-sign-out fa-fw"></span>&nbsp;&nbsp;<a href="<?php echo site_url('permission/logout')?>">Logout</a></li>
    </ul>

    <h4>Pengaturan Konten</h4>
    
    <ul class="list-group">
        <li class="list-group-item">
            <a role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><span class="fa fa-check-square-o fa-fw"></span>&nbsp;&nbsp;Pengaturan Soal</a>
            
            <div class="<?php echo (in_array($this->uri->segment(2), ['question', 'group'])) ? '' : 'collapse';?>" id="collapseExample">
                <ul class="list-group child-menu">
                    <li class="list-group-item <?php echo ($this->uri->segment(2) == 'question') ? 'active' : '';?>"><a href="<?php echo site_url('admin/question')?>">Daftar Soal</a></li>
                    <li class="list-group-item <?php echo ($this->uri->segment(2) == 'group') ? 'active' : '';?>"><a href="<?php echo site_url('admin/group')?>">Daftar Kelompok Soal</a></li>
                </ul>
            </div>
        </li>
        <li class="list-group-item <?php echo ($this->uri->segment(2) == 'school') ? 'active' : '';?>"><span class="fa fa-bank fa-fw"></span>&nbsp;&nbsp;<a href="<?php echo site_url('admin/school')?>">Pengaturan Sekolah</a></li>
        <li class="list-group-item <?php echo ($this->uri->segment(2) == 'student') ? 'active' : '';?>"><span class="fa fa-users fa-fw"></span>&nbsp;&nbsp;<a href="<?php echo site_url('admin/student')?>">Pengaturan Siswa</a></li>
        <li class="list-group-item">
            <a role="button" data-toggle="collapse" href="#angket" aria-expanded="false" aria-controls="collapseExample"><span class="fa fa-bullhorn fa-fw"></span>&nbsp;&nbsp;Pengaturan Angket</a>
            
            <div class="<?php echo (in_array($this->uri->segment(2), ['SurveyQuestion', 'survey'])) ? '' : 'collapse';?>" id="angket">
                <ul class="list-group child-menu">
                    <li class="list-group-item <?php echo ($this->uri->segment(2) == 'SurveyQuestion') ? 'active' : '';?>"><a href="<?php echo site_url('admin/SurveyQuestion')?>">Daftar Pertanyaan</a></li>
                    <li class="list-group-item <?php echo ($this->uri->segment(2) == 'survey') ? 'active' : '';?>"><a href="<?php echo site_url('admin/survey')?>">Daftar Angket</a></li>
                    <li class="list-group-item <?php echo ($this->uri->segment(2) == 'SurveyResult') ? 'active' : '';?>"><a href="<?php echo site_url('admin/SurveyResult')?>">Laporan Angket</a></li>
                </ul>
            </div>
        </li>
        <li class="list-group-item <?php echo ($this->uri->segment(2) == 'report') ? 'active' : '';?>"><span class="fa fa-book fa-fw"></span>&nbsp;&nbsp;<a href="<?php echo site_url('admin/report')?>">Laporan Hasil Uji</a></li>
    </ul>
</section>