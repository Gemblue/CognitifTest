<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Uji Meta Kognitif</title>
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">
    <link href="<?php echo base_url('resources/theme/simple/plugins/bootstrap/css/bootstrap.css');?>" rel="stylesheet">
    <link href="https://bootswatch.com/3/lumen/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('resources/theme/simple/css/admin.css');?>" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    
    <script src="<?php echo base_url('resources/theme/simple/plugins');?>/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url('resources/theme/simple/plugins');?>/bootstrap/js/bootstrap.js"></script>

    <!-- include summernote css/js -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    
</head>

<body>

<input type="hidden" id="base_url" value="<?php echo base_url();?>" />