
<script>
var base_url = $('#base_url').val();

$('.modal').on('shown.bs.modal', function () {
   $('.rich-editor-modal').summernote({ 
        height: 200,
        dialogsInBody: true
    });
});

$('.rich-editor').summernote({ 
    height: 500,
    dialogsInBody: true
});

setInterval(function(){ 
    $('.message').fadeOut();
}, 3000);

</script>

</body>

</html>