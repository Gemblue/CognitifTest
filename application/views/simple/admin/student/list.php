<?php $this->load->view($this->theme . '/admin/partials/header.php');?>

<div class="container">
    <?php $this->load->view($this->theme . '/admin/partials/logo.php');?>

    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view($this->theme . '/admin/partials/sidebar.php');?>
        </div>
        <div class="col-md-9">
            <div class="row">    
                <div class="col-md-2">
                    <span class="fa fa-graduation-cap icon-header"></span>
                </div>
                <div class="col-md-10">
                    <h4>Pengaturan Siswa</h4>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="#">Beranda</a>&nbsp;&nbsp;<span class="fa fa-angle-right"></span></li>
                            <li>Pengaturan Siswa</li>
                        </ul>
                    </div>
                </div>
            </div>

            <hr />

            <div class="row">
                <div class="col-md-6">
                    <h4>Daftar Siswa</h4>
                </div>    
                <div class="col-md-6">
                    <a href="#" class="btn btn-info btn-add-student pull-right"><span class="fa fa-plus"></span> Tambah</a>
                </div>    
            </div>
            
            <div class="box margin-md-top">
                
                <form class="form-inline margin-md-bottom" method="get" action="<?php echo current_url();?>">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group pull-left">
                                <input type="number" name="sort" class="form-control" value="10" placeholder="Sort" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group pull-right">
                                <div class="input-group">
                                    <input type="text" name="keyword" class="form-control" value="<?php echo $this->input->get('keyword')?>" placeholder="Search keyword .."/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">Search</button>
                                    </span>
                                </div><!-- /input-group -->
                            </div>
                        </div>
                    </div>
                </form>
                
                <?php echo $this->session->flashdata('message');?>

                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Nama Siswa</th>
                        <th>Sekolah</th>
                        <th>NISN</th>
                        <th>Terdaftar</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    
                    <?php 
                    $num = 1; 
                    foreach($students as $student) 
                    {
                        ?>
                        <tr>
                            <td><?php echo $num;?></td>
                            <td><?php echo $student->name;?></td>
                            <td><?php echo $student->school_name?></td>
                            <td><?php echo $student->nisn?></td>
                            <td><?php echo $student->created_at?></td>
                            <td>
                                <?php if ($student->status == 'inactive') :?>
                                    <span class="label label-danger">non active</span>
                                <?php else:?>
                                    <span class="label label-success">active</span>
                                <?php endif;?>
                            </td>
                            <td>
                                <a href="#" class="btn btn-info btn-edit-student btn-xs" data-id="<?php echo $student->id?>"><span class="fa fa-pencil"></span>&nbsp;&nbsp;Edit</a>
                                <a href="<?php echo base_url('admin/student/remove/' . $student->id)?>" class="btn btn-danger btn-xs" onclick="return confirm('Serius ingin dihapus?');"><span class="fa fa-trash"></span>&nbsp;&nbsp;Hapus</a>
                            </td>
                        </tr>
                        <?php 
                        $num++; 
                    }
                    ?>
                </table>

                <div class="pagination">
                    <?php echo $links;?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="studentModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah/Edit Siswa</h4>
            </div>
            <div class="modal-body">
                <div class="message"></div>
                
                <input type="hidden" id="id" />

                <div class="form-group">
                    <label for="">Nama</label>
                    <input type="text" id="name" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="">Username</label>
                    <input type="text" id="username" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" id="email" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="">NISN</label>
                    <input type="text" id="nisn" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="">Status</label>
                    <select id="status" class="form-control">
                        <option value="" selected>Pilih ..</option>
                        <option value="active" selected>Active</option>
                        <option value="inactive">Non Active</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Sekolah</label>
                    <select id="school_id" class="form-control">
                        <option value="" selected>Pilih ..</option>
                        <?php foreach($schools as $school) :?>
                            <option value="<?php echo $school->id?>" <?php echo ($school->id == $this->input->get('school_id')) ? 'selected' : ''; ?>><?php echo $school->name?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Jenis Kelamin</label>
                    <select id="jenis_kelamin" class="form-control">
                        <option value="" selected>Pilih ..</option>
                        <option value="Laki-laki" selected>Laki-laki</option>
                        <option value="Perempuan">Perempuan</option>
                    </select>
                </div>
                <hr>
                <div class="form-group">
                    <label for="">Password</label>
                    <input type="password" id="password" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="">Confirm Password</label>
                    <input type="password" id="confirm_password" class="form-control" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary btn-save">Simpan</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
var base_url = $('#base_url').val();
var action = 'insert';

$('.btn-add-student').click(function(){
    action = 'insert';

    $('#studentModal').modal('show');

    return false;
});

$('.btn-edit-student').click(function(){
    action = 'update';

    $('.message').html('');

    var id = $(this).attr('data-id');

    $.getJSON( base_url + 'admin/student/detail/' + id, function( data ) {
        
        $('#id').val(data.id);
        $('#username').val(data.username);
        $('#email').val(data.email);
        $('#name').val(data.name);
        $('#nisn').val(data.nisn);
        $('#status').val(data.status);
        $('#school_id').val(data.school_id);
        $('#jenis_kelamin').val(data.jenis_kelamin);
        
        $('#studentModal').modal('show');        
    });

    return false;
});

$('.btn-save').click(function(){
    var id = $('#id').val();
    var name = $('#name').val();
    var username = $('#username').val();
    var email = $('#email').val();
    var nisn = $('#nisn').val();
    var status = $('#status').val();
    var school_id = $('#school_id').val();
    var jenis_kelamin = $('#jenis_kelamin').val();
    var password = $('#password').val();
    
    $('.btn-save').html('Loading ..');
    
    $.post( base_url + 'admin/student/' + action, { id:id, username:username, email:email, password:password, status:status, name: name, nisn: nisn, school_id: school_id, jenis_kelamin: jenis_kelamin })
    .done(function( data ) {
        
        $('.btn-save').html('Simpan');
        
        if (data.status == 'failed') {
            $('.message').html('<div class="alert alert-danger">'+ data.message +'</div>');
        } else {
            alert(data.message);
            location.reload();
        }
    });
});
</script>

<?php $this->load->view($this->theme . '/admin/partials/footer.php');?>