<?php $this->load->view($this->theme . '/admin/partials/header.php');?>

<div class="container">
    <?php $this->load->view($this->theme . '/admin/partials/logo.php');?>

    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view($this->theme . '/admin/partials/sidebar.php');?>
        </div>
        <div class="col-md-9">
            <div class="row">    
                <div class="col-md-2">
                    <span class="fa fa-graduation-cap icon-header"></span>
                </div>
                <div class="col-md-10">
                    <h4>Pengaturan Sekolah</h4>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="#">Beranda</a>&nbsp;&nbsp;<span class="fa fa-angle-right"></span></li>
                            <li>Pengaturan Sekolah</li>
                        </ul>
                    </div>
                </div>
            </div>

            <hr />

            <div class="row">
                <div class="col-md-6">
                    <h4>Daftar Sekolah</h4>
                </div>    
                <div class="col-md-6">
                    <a href="#" class="btn btn-info btn-add-school pull-right"><span class="fa fa-plus"></span> Tambah</a>
                </div>    
            </div>
            
            <div class="box margin-md-top">
                
                <form class="form-inline margin-md-bottom" method="get" action="<?php echo current_url();?>">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group pull-left">
                                <input type="number" name="sort" class="form-control" value="10" placeholder="Sort" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group pull-right">
                                <div class="input-group">
                                    <input type="text" name="keyword" class="form-control" value="<?php echo $this->input->get('keyword')?>" placeholder="Search keyword .."/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">Search</button>
                                    </span>
                                </div><!-- /input-group -->
                            </div>
                        </div>
                    </div>
                </form>
                
                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Nama Sekolah</th>
                        <th>NPSN</th>
                        <th>Siswa Terkait</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    
                    <?php 
                    $num = 1; 
                    foreach($schools as $school) 
                    {
                        $connectedStudents = $this->School_model->getTotalStudents($school->id);
                        ?>
                        <tr>
                            <td><?php echo $num;?></td>
                            <td><?php echo $school->name;?></td>
                            <td><?php echo $school->npsn;?></td>
                            <td><?php echo $connectedStudents;?></td>
                            <td>
                                <?php if ($school->status == 'inactive') :?>
                                    <span class="label label-danger"><?php echo $school->status;?></span>
                                <?php else:?>
                                    <span class="label label-success"><?php echo $school->status;?></span>
                                <?php endif;?>
                            </td>
                            <td>
                                <a href="#" class="btn btn-info btn-edit-school btn-xs" data-id="<?php echo $school->id;?>"><span class="fa fa-pencil"></span>&nbsp;&nbsp;Edit</a>
                                <a href="<?php echo base_url('admin/group/remove/' . $school->id)?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span>&nbsp;&nbsp;Hapus</a>
                            </td>
                        </tr>
                        <?php 
                        $num++; 
                    }
                    ?>
                </table>

                <div class="pagination">
                    <?php echo $links;?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="schoolModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah/Edit Sekolah</h4>
            </div>
            <div class="modal-body">
                <div class="message"></div>
                
                <input type="hidden" id="id" />

                <div class="form-group">
                    <label for="">Nama Sekolah</label>
                    <input type="text" id="name" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="">NPSN</label>
                    <input type="text" id="npsn" class="form-control" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary btn-save">Simpan</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
var base_url = $('#base_url').val();
var action = 'insert';

$('.btn-add-school').click(function(){
    action = 'insert';

    $('#schoolModal').modal('show');  
});

$('.btn-edit-school').click(function(){
    action = 'update';

    $('.message').html('');

    var id = $(this).attr('data-id');

    $.getJSON( base_url + 'admin/school/detail/' + id, function( data ) {
        
        $('#id').val(data.id);
        $('#name').val(data.name);
        $('#npsn').val(data.npsn);
        
        $('#schoolModal').modal('show');        
    });

    return false;
});

$('.btn-save').click(function(){
    var id = $('#id').val();
    var name = $('#name').val();
    var npsn = $('#npsn').val();

    $('.btn-save').html('Loading ..');
    
    $.post( base_url + 'admin/school/' + action, { id:id, name: name, npsn: npsn, status: 'active' })
    .done(function( data ) {
        
        $('.btn-save').html('Simpan');
        
        if (data.status == 'failed') {
            $('.message').html('<div class="alert alert-danger">'+ data.message +'</div>');
        } else {
            alert(data.message);
            location.reload();
        }
    });
});
</script>

<?php $this->load->view($this->theme . '/admin/partials/footer.php');?>