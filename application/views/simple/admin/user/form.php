<?php $this->load->view('admin/partials/header.php');?>

<?php $this->load->view('admin/partials/sidebar.php');?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>USER</h2>
        </div>

        <!-- Basic Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <?php echo $title_page; ?>
                            <?php if ($form_type == 'edit'):?>
                                <small>Registered at <?php echo time_ago($result->created_at); ?></small>
                            <?php endif;?>                       
                        </h2>
                    </div>
                            
                    <div class="body">
                        
                        <?php echo $this->session->flashdata('message');?>

                        <form id="post-form" method="post" action="<?php echo ($form_type == 'new' ? site_url('admin/user/insert') : site_url('admin/user/update'));?>" enctype="multipart/form-data">
                            
                            <?php if ($form_type == 'edit') :?>
                                <input type="hidden" name="user_id" value="<?php echo (isset($result->id) ? $result->id : ''); ?>"/>
                            <?php endif;?>
                            
                            <h4>General</h4>
                            <hr />

                            <div class="form-group">
                                <label>NISN</label>
                                <div class="form-line">
                                    <input type="text" name="nisn" value="<?php echo (isset($result->nisn) ? $result->nisn : ''); ?>" class="form-control"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Name</label>
                                <div class="form-line">
                                    <input type="text" name="name" value="<?php echo (isset($result->name) ? $result->name : ''); ?>" class="form-control"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Username</label>
                                <div class="form-line">
                                    <input type="text" name="username" value="<?php echo (isset($result->username) ? $result->username : ''); ?>" class="form-control"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label>Handphone</label>
                                <div class="form-line">
                                    <input type="text" name="handphone" value="<?php echo (isset($result->handphone) ? $result->handphone : ''); ?>" class="form-control"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label>Email</label>
                                <div class="form-line">
                                    <input type="text" name="email" value="<?php echo (isset($result->email) ? $result->email : '');?>" class="form-control" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label>Jenis Kelamin</label>
                                <select name="jenis_kelamin" class="form-control">
                                    <option value="Laki-laki">Laki-laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Role</label>
                                <select name="role_id" class="form-control">
                                    <?php foreach ($roles as $r) :?>
                                        <option <?php echo (isset($result->role_id) && $r->id == $result->role_id) ? 'selected' : '';?> value="<?php echo $r->id?>"><?php echo $r->role_name?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>

                            <h4 style="margin-top:50px;">Setting Password</h4>
                            <hr />

                            <div class="form-group">
                                <label>Password</label>
                                <div class="form-line">
                                    <input type="password" name="password" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <div class="form-line">
                                    <input type="password" name="confirm_password" class="form-control"/>
                                </div>
                            </div>
                            
                            <div style="margin-top:30px;"></div>

                            <button type="submit" class="btn btn-success">Save</button>

                            <?php if ($form_type == 'edit') :?>
                                <?php if ($result->status == 'inactive'):?>
                                    <a href="<?php echo site_url('admin/user/activate/' . $result->id);?>" class="btn btn-success">Take back</a>
                                <?php else:?>
                                    <a href="<?php echo site_url('admin/user/block/' . $result->id);?>" class="btn btn-danger">Block</a>
                                <?php endif;?>
                                <a href="<?php echo site_url('user/profile/' . $result->username)?>" class="btn btn-info" target="_blank">View</a>
                            <?php endif;?>

                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Table -->

    </div>
</section>

<?php $this->load->view('admin/partials/footer.php');?>