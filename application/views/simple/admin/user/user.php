<?php $this->load->view('admin/partials/header.php');?>

<?php $this->load->view('admin/partials/sidebar.php');?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>USER</h2>
        </div>

        <!-- Basic Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            USER LIST
                            <small>User registered in this platform</small>
                        </h2>
                    </div>

                    <div class="body table-responsive">

                        <div class="row">
                            <div class="col-md-6">
                                <form id="form-search" method="post" action="<?php echo site_url('admin/user/search'); ?>" enctype="application/x-www-form-urlencoded" class="form-search">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="input-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="keyword" placeholder="Search Name ...">
                                                </div>
                                                
                                            </div><!-- /input-group -->
                                        </div><!-- /.col-lg-6 -->
                                    </div><!-- /.row -->
                                </form>
                            </div>
                            <div class="col-md-6">
                                <div class="pull-right">
                                    <a href="<?php echo site_url('admin/user/add');?>" class="btn btn-primary">Insert User</a>
                                </div>
                            </div>
                        </div>

                        <?php echo $this->session->flashdata('message');?>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <td>NISN</td>
                                    <td>NAME</td>
                                    <td>USERNAME</td>
                                    <td>HANDPHONE</td>
                                    <td>EMAIL</td>
                                    <td>STATUS</td>
                                    <td>REGISTERED</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>

                                <?php foreach($results as $row) :?>
                                    <tr>
                                        <th scope="row"><?php echo $no;?></th>
                                        <td><?php echo $row->nisn; ?></td>
                                        <td>
                                            <?php echo $row->name; ?>
                                            
                                            <?php if ($row->status == 'inactive' || $row->status == 'pending'): ?>
                                                <div class="margin-md-top">
                                                    <a class="btn btn-xs btn-success" onclick="return confirm('are you sure?')" href="<?php echo site_url('admin/user/activate/' . $row->id); ?>">Set active</a> 
                                                    <a class="btn btn-xs btn-danger" onclick="return confirm('are you sure?')" href="<?php echo site_url('admin/user/delete/' . $row->id); ?>">Delete</a>
                                                </div>
                                            <?php else: ?>
                                                <div class="margin-md-top">
                                                    <a class="btn btn-xs btn-success" href="<?php echo site_url('admin/user/edit/' . $row->id); ?>">Edit</a>
                                                    <a class="btn btn-xs btn-danger" onclick="return confirm('are you sure?')" href="<?php echo site_url('admin/user/block/' . $row->id); ?>">Block</a>
                                                    <a class="btn btn-xs btn-info" href="<?php echo site_url('user/profile/' . $row->username); ?>" target="_blank">View</a>
                                                </div>
                                            <?php endif ?>
                                        </td>
                                        <td><?php echo $row->username; ?></td>
                                        <td><?php echo $row->handphone; ?></td>
                                        <td><?php echo $row->email; ?></td>
                                        <td>
                                            <?php if ($row->status == 'active'):?>
                                                <span class="label label-success"><?php echo $row->status;?></span>
                                            <?php else:?>
                                                <span class="label label-danger"><?php echo $row->status;?></span>
                                            <?php endif;?>
                                        </td>
                                        <td><?php echo $row->created_at; ?></td>
                                    </tr>
                                    
                                    <?php $no++; ?>

                                <?php endforeach;?>
                            </tbody>
                        </table>

                        <?php if(isset($pagination)) : ?>
                            <div class="pagination">
                                <?php echo $pagination; ?>
                            </div>
                        <?php endif; ?>
	
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Table -->

    </div>
</section>

<?php $this->load->view('admin/partials/footer.php');?>