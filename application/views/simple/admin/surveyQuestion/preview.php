@include('admin.partials.header')
@include('admin.partials.sidebar')

<link href='https://cdnjs.cloudflare.com/ajax/libs/simplemde/1.11.2/simplemde.min.css' rel='stylesheet' type='text/css'>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		@include('admin.partials.breadcrumb')
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Import Preview for <b>{{ $course->course_title }}</b> in <b>{{ $category->category_name }}</b>
				</div>
				
				<div class="panel-body">

					<ol>
						@foreach ($questions as $question)
							<li>
								<p>{!! $Markdown->parse($question['soal']) !!}<p>

								<ul>
								@foreach($question['pilihan'] as $index => $value)
									<li>{{ $value }} {!! ($index == $question['jawaban']) ? '<span class="label label-success">Benar</span>' : '' !!}</li>
								@endforeach
								</ul>
							</li>
						@endforeach
					</ol>

					<br/>

					<a href="{{ url('admin/course/question/import_do?' . $_SERVER['QUERY_STRING']) }}" class="btn btn-primary">Save</a>
				</div>

			</div><!-- /.col-->
		</div>
		
	</div> <!-- /.row -->

</div> <!--/.main-->	

@include('admin.partials.footer')