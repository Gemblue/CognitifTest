<?php $this->load->view($this->theme . '/admin/partials/header.php');?>

<div class="container">
    <?php $this->load->view($this->theme . '/admin/partials/logo.php');?>

    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view($this->theme . '/admin/partials/sidebar.php');?>
        </div>
        <div class="col-md-9">
            <div class="row">    
                <div class="col-md-2">
                    <span class="fa fa-graduation-cap icon-header"></span>
                </div>
                <div class="col-md-10">
                    <h4>Daftar Pertanyaan</h4>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="#">Beranda</a>&nbsp;&nbsp;<span class="fa fa-angle-right"></span></li>
                            <li>Daftar Pertanyaan</li>
                        </ul>
                    </div>
                </div>
            </div>

            <hr />

            <h4>Pencarian</h4>

            <div class="box">
                <div class="row">
                    <div class="col-md-10">
                        <form class="form-inline" method="get" action="<?php echo current_url();?>">
                            <div class="form-group">
                                <input type="text" name="keyword" class="form-control" value="<?php echo $this->input->get('keyword');?>" placeholder="Judul Pertanyaan"/>
                            </div>
                            <div class="form-group">
                                <select name="group_id" class="form-control">
                                    <option value="" selected>Pilih Angket ..</option>
                                    <?php foreach($groups as $group) :?>
                                        <option value="<?php echo $group->id;?>" <?php echo ($group->id == $this->input->get('group_id')) ? 'selected' : ''; ?>><?php echo $group->title?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-success">Cari</button>
                        </form>
                    </div>
                    <div class="col-md-2">
                        <a href="#" class="btn btn-info btn-add-question pull-right"><span class="fa fa-plus"></span> Tambah</a>
                    </div>
                </div>
            </div>

            <h4>Soal</h4>

            <div class="box">
                <form class="form-inline margin-md-bottom" method="get" action="<?php echo current_url();?>">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group pull-left">
                                <input type="number" name="sort" class="form-control" value="10" placeholder="Sort" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group pull-right">
                                <div class="input-group">
                                    <input type="text" name="keyword" class="form-control" value="<?php echo $this->input->get('keyword')?>" placeholder="Search keyword .."/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">Search</button>
                                    </span>
                                </div><!-- /input-group -->
                            </div>
                        </div>
                    </div>
                </form>
                
                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Judul Pertanyaan</th>
                        <th>Angket</th>
                        <th>Tanggal Dibuat</th>
                        <th>Tanggal Diedit</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    
                    <?php $num = 1; foreach($questions as $question) :?>
                        <tr>
                            <td><?php echo $num;?></td>
                            <td><?php echo $question['question_title'];?></td>
                            <td><?php echo $question['title'];?></td>
                            <td><?php echo $question['created_at'];?></td>
                            <td><?php echo $question['updated_at'];?></td>
                            <td>
                                <?php if ($question['question_status'] == 'draft') :?>
                                    <span class="label label-danger"><?php echo $question['question_status'];?></span>
                                <?php else:?>
                                    <span class="label label-success"><?php echo $question['question_status'];?></span>
                                <?php endif;?>
                            </td>
                            <td width="20%">
                                <a href="<?php echo base_url('admin/SurveyQuestion/edit/' . $question['question_id'])?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span>&nbsp;&nbsp;Edit</a>
                                <a href="<?php echo base_url('admin/SurveyQuestion/remove/' . $question['question_id'])?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span>&nbsp;&nbsp;Hapus</a>
                            </td>
                        </tr>
                    <?php $num++; endforeach;?>
                </table>

                <div class="pagination">
                    <?php echo $links;?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="questionModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Pertanyaan</h4>
            </div>
            <div class="modal-body">
                <div class="message"></div>
                
                <div class="form-group">
                    <label for="">Judul Pertanyaan</label>
                    <input type="text" id="question_title" class="form-control" />
                </div>
                
                <div class="form-group">
                    <label for="">Angket</label>
                    <select id="group_id" class="form-control">
                        <?php foreach($groups as $group) :?>
                            <option value="<?php echo $group->id;?>"><?php echo $group->title;?></option>
                        <?php endforeach;?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Rincian Pertanyaan</label>
                    <textarea id="question_content" class="form-control rich-editor-modal" row="10"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary btn-save">Simpan</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
var base_url = $('#base_url').val();

$('.btn-add-question').click(function(){
    $('#questionModal').modal('show');  
});

$('.btn-save').click(function(){
    var question_title = $('#question_title').val();
    var group_id = $('#group_id').val();
    var question_content = $('#question_content').val();
    
    $('.btn-save').html('Loading ..');
    
    $.post( base_url + 'admin/SurveyQuestion/insert', { question_title: question_title, group_id: group_id, question_content:question_content })
    .done(function( data ) {
        
        $('.btn-save').html('Simpan');
        
        if (data.status == 'failed') {
            $('.message').html('<div class="alert alert-danger">'+ data.message +'</div>');
        } else {
            alert(data.message);
            location.reload();
        }
    });
});
</script>

<?php $this->load->view($this->theme . '/admin/partials/footer.php');?>