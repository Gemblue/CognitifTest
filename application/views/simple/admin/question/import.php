@include('admin.partials.header')
@include('admin.partials.sidebar')

<link href='https://cdnjs.cloudflare.com/ajax/libs/simplemde/1.11.2/simplemde.min.css' rel='stylesheet' type='text/css'>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		@include('admin.partials.breadcrumb')
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Setup Questions for <b>{{ $course->course_title }}</b> in <b>{{ $category->category_name }}</b>
				</div>
				
				<div class="panel-body">

					@if(Session::has('message'))   
						<div class="alert alert-{{ Session::get('status')}}">
							{{ Session::get('message')}}
						</div>
					@endif
					
					<form method="post" action="{{ url('admin/course/question/import_action') }}" enctype="multipart/form-data">
						{{ csrf_field() }}
						
						<input type="hidden" name="course_id" value="{{ $course->id }}">
						<input type="hidden" name="category_id" value="{{ $category->id }}">
						
						<div class="form-group">
							<label>Choose question file ..</label>
							<input type="file" name="question_file" />
							<br/>
							<button type="submit" class="btn btn-primary">Import</button>
						</div>
					</form>
				</div>
			</div><!-- /.col-->
		</div>
		
	</div> <!-- /.row -->

</div> <!--/.main-->	

@include('admin.partials.footer')