<?php $this->load->view($this->theme . '/admin/partials/header.php');?>

<div class="container">
    <?php $this->load->view($this->theme . '/admin/partials/logo.php');?>

    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view($this->theme . '/admin/partials/sidebar.php');?>
        </div>
        <div class="col-md-9">
            <div class="row">    
                <div class="col-md-2">
                    <span class="fa fa-graduation-cap icon-header"></span>
                </div>
                <div class="col-md-10">
                    <h4>Daftar Soal</h4>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="#">Beranda</a>&nbsp;&nbsp;<span class="fa fa-angle-right"></span></li>
                            <li><a href="#">Pengaturan Soal</a>&nbsp;&nbsp;<span class="fa fa-angle-right"></span></li>
                            <li>Daftar Soal</li>
                        </ul>
                    </div>
                </div>
            </div>

            <hr />

            <h4>Detail Soal</h4>

            <div class="box">
                <?php echo $this->session->flashdata('message');?>

                <form action="<?php echo $action;?>" method="post">
                    <?php if (isset($question->question_id)) :?>
                        <input type="hidden" name="question_id" id="question_id" value="<?php echo $question->question_id;?>" />
                    <?php endif;?>

                    <div class="form-group">
                        <label for="">Judul Pertanyaan</label>
                        <input type="text" name="question_title" class="form-control" placeholder="Judul Pertanyaan" value="<?php echo (isset($question->question_title)) ? $question->question_title : '';?>"/>
                    </div>
                    <div class="form-group">
                        <label for="">Kelompok Soal</label>
                        <select name="group_id" class="form-control">
                            <?php foreach($groups as $group) :?>
                                <option value="<?php echo $group->id;?>" <?php echo (isset($question->group_id) && $question->group_id == $group->id) ? 'selected' : '';?> ><?php echo $group->title;?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Rincian Soal</label>
                        <textarea name="question_content" row="10" class="form-control rich-editor" placeholder="Rincian soal .."><?php echo (!empty($question->question_content)) ? $question->question_content : '';?></textarea>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-info"><span class="fa fa-save"></span> Simpan</button>
                    </div>
                </form>
            </div>

            <?php if (isset($question->question_id)) :?>

                <h4>Opsi Jawaban</h4>

                <div class="box">
                    <div class="margin-md-bottom">
                        <a href="#" class="btn btn-info btn-add-opsi pull-right margin-md-bottom"><span class="fa fa-plus"></span> Tambah</a>
                    </div>

                    <table class="table table-bordered">
                        <tr>
                            <th>Status</th>
                            <th>Jawaban</th>
                            <th>Gambar</th>
                            <th></th>
                        </tr>
                        
                        <?php
                        $options = $this->Question_model->getOptionsByQuestion($question->question_id);

                        foreach($options as $option) :?>
                            <tr>
                                <td>
                                    <?php if ($option->is_right) :?>
                                        <span class="label label-success">True</span>
                                    <?php else:?>
                                        <span class="label label-danger">False</span>
                                    <?php endif;?>
                                </td>
                                <td><?php echo $option->option_content;?></td>
                                <td>-</td>
                                <td>
                                    <a href="#" class="btn btn-info btn-edit-opsi btn-xs" data-id="<?php echo $option->option_id;?>"><span class="fa fa-pencil"></span>&nbsp;&nbsp;Edit</a>
                                    <a href="#" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span>&nbsp;&nbsp;Hapus</a>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </table>
                </div>

                <h4>Daftar Petunjuk</h4>

                <div class="box">
                    <div class="margin-md-bottom">
                        <a href="#" class="btn btn-info btn-add-hint pull-right margin-md-bottom"><span class="fa fa-plus"></span> Tambah</a>
                    </div>

                    <table class="table table-bordered">
                        <tr>
                            <th>Kategori</th>
                            <th>Hint</th>
                            <th>Urutan</th>
                            <th>Gambar</th>
                            <th></th>
                        </tr>
                        
                        <?php 
                        $hints = $this->Question_model->getHintsByQuestion($question->question_id);

                        foreach($hints as $hint) :?>
                            <tr>
                                <td><?php echo $hint->hint_category;?></td>
                                <td><?php echo $hint->hint_content;?></td>
                                <td><?php echo $hint->hint_sort;?></td>
                                <td>-</td>
                                <td>
                                    <a href="#" class="btn btn-info btn-edit-hint btn-xs" data-id="<?php echo $hint->hint_id;?>"><span class="fa fa-pencil"></span>&nbsp;&nbsp;Edit</a>
                                    <a href="#" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span>&nbsp;&nbsp;Hapus</a>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </table>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>

<div id="opsiModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Opsi</h4>
            </div>
            <div class="modal-body">
                <div class="message"></div>
            
                <input type="hidden" id="option_id" />
                
                <div class="form-group">
                    <label for="">Jawaban Opsi</label>
                    <textarea id="option_content" class="form-control rich-editor-modal"></textarea>
                </div>
                <div class="form-group">
                    <label for="">Status Jawaban</label>
                    <select id="is_right" class="form-control">
                        <option value="1">True</option>
                        <option value="0">False</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Gambar</label>
                    <input type="file" class="form-control"/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary btn-save-opsi">Simpan</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="hintModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Hint</h4>
            </div>
            <div class="modal-body">
                <div class="message"></div>
            
                <input type="hidden" id="hint_id" />
                
                <div class="form-group">
                    <label for="">Petunjuk/Hint</label>
                    <textarea id="hint_content" class="form-control rich-editor-modal"></textarea>
                </div>
                <div class="form-group">
                    <label for="">Kategori</label>
                    <select id="hint_category" class="form-control">
                        <option value="Deklaratif">Deklaratif</option>
                        <option value="Prosedural">Prosedural</option>
                        <option value="Strategi">Strategi</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Sort</label>
                    <input type="number" id="hint_sort" class="form-control"/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary btn-save-hint">Simpan</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
var base_url = $('#base_url').val();
var action = 'insert';

/**
 * Option Management
 */
$('.btn-add-opsi').click(function(){
    action = 'insert';

    $('#opsiModal').modal('show');

    return false;
});

$('.btn-edit-opsi').click(function(){
    action = 'update';

    $('.message').html('');

    var id = $(this).attr('data-id');

    $.getJSON( base_url + 'admin/option/detail/' + id, function( data ) {
        
        $('#option_id').val(data.option_id);
        $('#option_content').val(data.option_content);
        $('#is_right').val(data.is_right);
        
        $('#opsiModal').modal('show');        
    });

    return false;
});

$('.btn-save-opsi').click(function(){
    var option_id = $('#option_id').val();
    var option_content = $('#option_content').val();
    var question_id = $('#question_id').val();
    var is_right = $('#is_right').val();

    $('.btn-save-opsi').html('Loading ..');
    
    $.post( base_url + 'admin/option/' + action, { option_id: option_id, question_id: question_id, option_content: option_content, is_right: is_right })
    .done(function( data ) {

        $('.btn-save-opsi').html('Simpan');
        
        if (data.status == 'failed') {
            $('.message').html('<div class="alert alert-danger">'+ data.message +'</div>');
        } else {
            alert(data.message);
            location.reload();
        }
    });
});

/**
 * Hint Management
 */
$('.btn-add-hint').click(function(){
    action = 'insert';

    $('#hintModal').modal('show');

    return false;
});

$('.btn-edit-hint').click(function(){
    action = 'update';
    
    $('.message').html('');

    var id = $(this).attr('data-id');

    $.getJSON( base_url + 'admin/hint/detail/' + id, function( data ) {
        
        $('#hint_id').val(data.hint_id);
        $('#hint_content').val(data.hint_content);
        $('#hint_category').val(data.hint_category);
        $('#hint_sort').val(data.hint_sort);
        
        $('#hintModal').modal('show');        
    });

    return false;
});

$('.btn-save-hint').click(function(){
    var hint_id = $('#hint_id').val();
    var question_id = $('#question_id').val();
    var hint_content = $('#hint_content').val();
    var hint_category = $('#hint_category').val();
    var hint_sort = $('#hint_sort').val();
    
    $('.btn-save-hint').html('Loading ..');
    
    $.post( base_url + 'admin/hint/' + action, { hint_id: hint_id, question_id: question_id, hint_content: hint_content, hint_category: hint_category, hint_sort: hint_sort })
    .done(function( data ) {

        $('.btn-save-hint').html('Simpan');
        
        if (data.status == 'failed') {
            $('.message').html('<div class="alert alert-danger">'+ data.message +'</div>');
        } else {
            alert(data.message);
            location.reload();
        }
    });
});
</script>

<?php $this->load->view($this->theme . '/admin/partials/footer.php');?>