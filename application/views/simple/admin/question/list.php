<?php $this->load->view($this->theme . '/admin/partials/header.php');?>

<div class="container">
    <?php $this->load->view($this->theme . '/admin/partials/logo.php');?>

    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view($this->theme . '/admin/partials/sidebar.php');?>
        </div>
        <div class="col-md-9">
            <div class="row">    
                <div class="col-md-2">
                    <span class="fa fa-graduation-cap icon-header"></span>
                </div>
                <div class="col-md-10">
                    <h4>Daftar Soal</h4>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="#">Beranda</a>&nbsp;&nbsp;<span class="fa fa-angle-right"></span></li>
                            <li><a href="#">Pengaturan Soal</a>&nbsp;&nbsp;<span class="fa fa-angle-right"></span></li>
                            <li>Daftar Soal</li>
                        </ul>
                    </div>
                </div>
            </div>

            <hr />

            <h4>Pencarian</h4>

            <div class="box">
                <div class="row">
                    <div class="col-md-10">
                        <form class="form-inline" method="get" action="<?php echo current_url();?>">
                            <div class="form-group">
                                <input type="text" name="keyword" class="form-control" value="<?php echo $this->input->get('keyword');?>" placeholder="Judul Pertanyaan"/>
                            </div>
                            <div class="form-group">
                                <select name="group_id" class="form-control">
                                    <option value="" selected>Pilih Kelompok Soal ..</option>
                                    <?php foreach($groups as $group) :?>
                                        <option value="<?php echo $group->id;?>" <?php echo ($group->id == $this->input->get('group_id')) ? 'selected' : ''; ?>><?php echo $group->title?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-success">Cari</button>
                        </form>
                    </div>
                    <div class="col-md-2">
                        <a href="<?php echo site_url('admin/question/add')?>" class="btn btn-info pull-right"><span class="fa fa-plus"></span> Tambah</a>
                    </div>
                </div>
            </div>

            <h4>Soal</h4>

            <div class="box">
                <form class="form-inline margin-md-bottom" method="get" action="<?php echo current_url();?>">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group pull-left">
                                <input type="number" name="sort" class="form-control" value="10" placeholder="Sort" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group pull-right">
                                <div class="input-group">
                                    <input type="text" name="keyword" class="form-control" value="<?php echo $this->input->get('keyword')?>" placeholder="Search keyword .."/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">Search</button>
                                    </span>
                                </div><!-- /input-group -->
                            </div>
                        </div>
                    </div>
                </form>
                
                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Judul Pertanyaan</th>
                        <th>Kelompok Soal</th>
                        <th>Tanggal Dibuat</th>
                        <th>Tanggal Diedit</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    
                    <?php $num = 1; foreach($questions as $question) :?>
                        <tr>
                            <td><?php echo $num;?></td>
                            <td><?php echo $question['question_title'];?></td>
                            <td><?php echo $question['title'];?></td>
                            <td><?php echo $question['created_at'];?></td>
                            <td><?php echo $question['updated_at'];?></td>
                            <td>
                                <?php if ($question['question_status'] == 'draft') :?>
                                    <span class="label label-danger"><?php echo $question['question_status'];?></span>
                                <?php else:?>
                                    <span class="label label-success"><?php echo $question['question_status'];?></span>
                                <?php endif;?>
                            </td>
                            <td width="20%">
                                <a href="<?php echo base_url('admin/question/edit/' . $question['question_id'])?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span>&nbsp;&nbsp;Edit</a>
                                <a href="<?php echo base_url('admin/question/remove/' . $question['question_id'])?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span>&nbsp;&nbsp;Hapus</a>
                            </td>
                        </tr>
                    <?php $num++; endforeach;?>
                </table>

                <div class="pagination">
                    <?php echo $links;?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var base_url = $('#base_url').val();

$('.btn-save').click(function(){
    var question_title = $('#question_title').val();
    var group_id = $('#group_id').val();
    var question_content = $('#question_content').val();
    
    $('.btn-save').html('Loading ..');
    
    $.post( base_url + 'admin/question/insert', { question_title: question_title, group_id: group_id, question_content:question_content })
    .done(function( data ) {
        
        $('.btn-save').html('Simpan');
        
        if (data.status == 'failed') {
            $('.message').html('<div class="alert alert-danger">'+ data.message +'</div>');
        } else {
            alert(data.message);
            location.reload();
        }
    });
});
</script>

<?php $this->load->view($this->theme . '/admin/partials/footer.php');?>