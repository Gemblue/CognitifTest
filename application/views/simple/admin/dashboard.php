<?php $this->load->view($this->theme . '/admin/partials/header.php');?>

<div class="container">
    <?php $this->load->view($this->theme . '/admin/partials/logo.php');?>

    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view($this->theme . '/admin/partials/sidebar.php');?>
        </div>
        <div class="col-md-9">
            <div class="row">    
                <div class="col-md-2">
                    <span class="fa fa-graduation-cap icon-header"></span>
                </div>
                <div class="col-md-10">
                    <h4>Insigh Metakognitif</h4>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="#">Beranda</a>&nbsp;&nbsp;<span class="fa fa-angle-right"></span></li>
                            <li>Insight</li>
                        </ul>
                    </div>
                </div>
            </div>

            <hr />

            <h4>Infografis</h4>

            <div class="box">
                <div class="row">
                    <div class="col-md-3">
                        <div class="well">
                            <b><?php echo $total_questions?></b> Soal
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="well">
                            <b><?php echo $total_group?></b> Kelompok Soal
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="well">
                            <b><?php echo $total_school?></b> Sekolah
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="well">
                            <b><?php echo $total_users?></b> Total Siswa
                        </div>
                    </div>
                </div>
                
            </div>    

            <div class="row">
                <div class="col-md-8">
                    <h4>Info Skor Siswa</h4>

                    <div class="box">
                        <?php if (!empty($results)) :?>
                            <table class="table table-bordered">
                                <tr>
                                    <th>Nama Siswa</th>
                                    <th>Kelompok Soal</th>
                                    <th>Sekolah</th>
                                    <th>Skor</th>
                                    <th></th>
                                </tr>
                                
                                <?php foreach($results as $result) :?>
                                    <tr>
                                        <td><?php echo $result->name;?></td>
                                        <td><?php echo $result->title;?></td>
                                        <td><?php echo $result->school_name;?></td>
                                        <td><?php echo $result->score;?></td>
                                        <td>
                                            <a href="<?php echo site_url('admin/report/detail/' . $result->result_id)?>" class="btn btn-info btn-xs"><span class="fa fa-search"></span>&nbsp;&nbsp;Detail</a>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                            </table>
                        <?php else:?>
                            Data tidak ditemukan ..
                        <?php endif;?>
                    </div>

                    <h4>Info Pertanyaan</h4>

                    <div class="box">
                        <table class="table table-bordered">
                            <tr>
                                <th>Judul Pertanyaan</th>
                                <th>Tanggal Dibuat</th>
                                <th>Tanggal Diedit</th>
                                <th>Status</th>
                            </tr>
                            
                            <?php foreach($questions as $question) :?>
                                <tr>
                                    <td><?php echo $question['question_title'];?></td>
                                    <td><?php echo $question['created_at'];?></td>
                                    <td><?php echo $question['updated_at'];?></td>
                                    <td>
                                        <?php if ($question['question_status'] == 'draft') :?>
                                            <span class="label label-danger"><?php echo $question['question_status'];?></span>
                                        <?php else:?>
                                            <span class="label label-success"><?php echo $question['question_status'];?></span>
                                        <?php endif;?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </table>
                    </div>
                </div>
                <div class="col-md-4">
                    <h4>Respon Angket Kuisioner</h4>
                </div>
            </div>
            
        </div>
    </div>
</div>


<?php $this->load->view($this->theme . '/admin/partials/footer.php');?>