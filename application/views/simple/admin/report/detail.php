<?php $this->load->view($this->theme . '/admin/partials/header.php');?>

<div class="container">
    <?php $this->load->view($this->theme . '/admin/partials/logo.php');?>

    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view($this->theme . '/admin/partials/sidebar.php');?>
        </div>
        <div class="col-md-9">
            <div class="row">    
                <div class="col-md-2">
                    <span class="fa fa-graduation-cap icon-header"></span>
                </div>
                <div class="col-md-10">
                    <h4>Rincian Hasil Uji</h4>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="#">Beranda</a>&nbsp;&nbsp;<span class="fa fa-angle-right"></span></li>
                            <li><a href="#">Laporan Hasil Uji Siswa</a>&nbsp;&nbsp;<span class="fa fa-angle-right"></span></li>
                            <li>Rincian Hasil Uji</li>
                        </ul>
                    </div>
                </div>
            </div>

            <hr />

            <h4>Informasi</h4>

            <div class="box">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Nama Siswa</label>
                            <br/><?php echo $result->name?>
                        </div>
                        <div class="form-group">
                            <label for="">Sekolah</label>
                            <br/><?php echo $result->school_name?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">NISN</label>
                            <br/><?php echo $result->nisn?>
                        </div>
                        <div class="form-group">
                            <label for="">Kelompok Soal</label>
                            <br/><?php echo $result->title?>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-4">
                                Skor Capaian
                                <div class="well"><?php echo $result->score;?></div>
                            </div>
                            <div class="col-md-4">
                                Waktu Pengerjaan
                                <div class="well"><?php echo secToHR($result->result_duration);?></div>
                            </div>
                            <div class="col-md-4">
                                Durasi Akses Hint
                                <div class="well"><?php echo secToHR($total_hint_duration);?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <h4>Daftar Jawaban</h4>

            <?php if (!empty($answer_logs)) :?>
                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Pertanyaan</th>
                        <th>Status Jawaban</th>
                        <th>Hint Dibuka</th>
                        <th>Total Durasi Hint</th>
                        <th></th>
                    </tr>
                    
                    <?php $num = 1; foreach($answer_logs as $answer_log) :?>
                        <tr>
                            <td><?php echo $num;?></td>
                            <td><?php echo $answer_log->question_content;?></td>
                            <td><?php echo ($answer_log->log_status) ? '<span class="label label-success">Benar</span>' : '<span class="label label-danger">Salah</span>';?></td>
                            <td><?php echo $this->Hint_model->getTotalHintLog($result->result_id, $answer_log->question_id);?> Kali</td>
                            <td><?php echo $this->Hint_model->getTotalDurationHintLog($result->result_id, $answer_log->question_id);?> Detik</td>
                            <td>
                                <a href="#" class="btn btn-info btn-detail-hint btn-xs" data-session-id="<?php echo $result->result_id;?>" data-question-id="<?php echo $answer_log->question_id;?>"><span class="fa fa-search"></span>&nbsp;&nbsp;Detail</a>
                            </td>
                        </tr>
                    <?php $num++; endforeach;?>                    
                </table>
            <?php else:?>
                <p>Belum ada jawaban sama sekali ..</p>
            <?php endif;?>
        </div>
    </div>
</div>

<div id="detailHintModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Catatan Waktu Akses Hint</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Hint Ke</th>
                            <th>Menit Akses</th>
                            <th>Petunjuk</th>
                            <th>Kategori</th>
                            <th>Durasi</th>
                        </tr>
                    </thead>
                    <tbody class="hint-detail">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
$('.btn-detail-hint').click(function(){
    
    var session_id = $(this).attr('data-session-id');
    var question_id = $(this).attr('data-question-id');

    $('.hint-detail').html('');

    $.getJSON( base_url + 'admin/hint/logs/' + session_id + '/' + question_id , function( data ) {

        var num = 1;
        var duration;

        $.each( data, function( key, value ) {
            
            if (value.hint_log_duration == null)
                duration = '0';
            else
                duration = value.hint_log_duration;

            var html =  '<tr>' +
                            '<td>' + num + '</td>' +
                            '<td>02.30</td>' +
                            '<td>'+ value.hint_content +'</td>' +
                            '<td>'+ value.hint_category +'</td>' +
                            '<td>'+ duration +' Detik</td>' +
                        '</tr>';
            
            $('.hint-detail').append(html);
            
            num++;
        });

        $('#detailHintModal').modal('show');
    });

    return false;
});
</script>

<?php $this->load->view($this->theme . '/admin/partials/footer.php');?>