<?php $this->load->view($this->theme . '/partials/header.php');?>

<h3><?php echo $group['title']?></h3>

<div class="margin-md-bottom">
    <span class="fa fa-info-circle"></span>&nbsp;&nbsp;&nbsp;Jawab dengan cara mengklik opsi yang tertera. Jika sudah yakin semua sudah dijawab silahkan klik tombol finish.
</div>

<div class="row">
    <div class="col-md-8">
        <div class="box">
            <h4>Pertanyaan</h4>
            
            <form id="form-exercise" action="<?php echo site_url('room/finish')?>" method="post">
                <?php $i=1; foreach ($questions as $question) :?>
                    <div style="display:none;" class="question question-<?php echo $i;?>">
                        <p><?php echo $question['question_content'];?></p>

                        <div class="option">
                            <?php
                            $options = $this->Question_model->getOptionsByQuestion($question['question_id']);    
                            if (!empty($options))
                            {
                                ?>
                                
                                <?php foreach($options as $option) :?>
                                    <div class="radio">
                                        <label><input type="radio" class="input-radio" name="<?php echo $question['question_id']?>_answer" value="<?php echo $option->option_id?>" data-number="<?php echo $i?>"><?php echo $option->option_content;?></label>
                                    </div>
                                <?php endforeach;?>
                                
                                <?php
                            }
                            else
                            {
                                ?>Belum ada opsi jawaban ..<?php
                            }
                            ?>
                        </div>

                        <hr />

                        <div class="row">
                            <div class="col-md-5">
                                <h3>Petunjuk :</h3>
                                <div class="alert alert-info">
                                    Apabila mengalami kesulitan dalam menyelesaikan soal, pilih bantuan petunjuk berikut ini untuk menemukan solusinya.
                                </div>
                            </div>
                            <div class="col-md-7">
                                <?php $hints = $this->Question_model->getHintsByQuestion($question['question_id']); ?>  
                                
                                <?php if (!empty($hints)) :?>
                                    <div class="margin-lg-top">
                                        <?php foreach($hints as $hint) :?>
                                            
                                            <?php
                                            $color = [
                                                'Deklaratif' => 'btn-warning',
                                                'Prosedural' => 'btn-success',
                                                'Strategi' => 'btn-danger',
                                            ];
                                            ?>
                                            
                                            <a href="#" class="btn <?php echo $color[$hint->hint_category]?> btn-show-hint" data-question-id="<?php echo $question['question_id'];?>" data-hint-id="<?php echo $hint->hint_id?>"><span class="fa fa-info-circle"></span>&nbsp;&nbsp;<?php echo $hint->hint_category?></a>
                                        <?php endforeach;?>
                                    </div>
                                <?php endif;?>
                            </div>
                        </div>
                        
                    </div>
                <?php $i++; endforeach;?>
            </form>
            
        </div>
    </div>
    <div class="col-md-4">

        <div class="box text-center">
            <h4>Navigasi</h4>
            
            <div class="navigation">
                <div style="margin-bottom:10px;">
                    <?php $i=1; foreach ($questions as $question) :?>
                        <a href="#" class="btn btn-default btn-sm btn-paging btn-paging-<?php echo $i;?>" data-number="<?php echo $i;?>"><?php echo $i?></a>
                        
                        <?php if ($i % 5 == 0) :?>
                            </div><div style="margin-bottom:10px;">         
                        <?php endif;?>

                    <?php $i++; endforeach;?>
                </div>
            </div>
            
            <div class="margin-md-top">
                <a href="#" class="btn btn-danger btn-prev"><span class="fa fa-angle-left"></span></a>
                <a href="#" class="btn btn-success btn-next"><span class="fa fa-angle-right"></span></a>
            </div>
            
        </div>
        <div class="mb-3 text-center">
            <div style="font-size:20px;">
                Durasi Tersisa :<br/>
                <b><span id="timer_duration"></span></b>
            </div>
            <div class="margin-md-top">
                <a href="#" class="btn btn-success btn-submit"><span class="fa fa-check"></span> Finish</a>
            </div>
        </div>
        
    </div>
</div>

<div id="hintModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Petunjuk</h4>
            </div>
            <div class="modal-body">
                <div id="timer" class="margin-md-bottom">Start ..</div>
                <div id="hint_content">Loading ..</div>            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<input type="hidden" id="total" value="<?php echo $total;?>"/>
<input type="hidden" id="duration" value="<?php echo $group['duration'];?>"/>

<script>
var page = 1;
var timer = 0;
var hint_id;
var hint_log_id;
var question_id;
var base_url = $('#base_url').val();
var total = $('#total').val();
var myTimer;
var duration = $('#duration').val();;

// Init
$('.question-1').fadeIn();
$('.btn-paging-1').addClass('btn-info');

// Question Js
$('.btn-paging').click(function(){
    var page = $(this).attr('data-number');

    $('.btn-paging').removeClass('btn-info');
    $('.btn-paging-' + page).addClass('btn-info');

    $('.question').hide();
    $('.question-' + page).fadeIn();

    return false;
});

$('.btn-prev').click(function(){
    
    if (page <= 1)
    {
        alert('Halaman tidak ditemukan ..');
    }
    else
    {
        page--;

        $('.btn-paging').removeClass('btn-info');
        $('.btn-paging-' + page).addClass('btn-info');

        $('.question').hide();
        $('.question-' + page).fadeIn();
    }

    return false;
});

$('.btn-next').click(function(){
     
    if (page >= total)
    {
        alert('Halaman tidak ditemukan ..');
    }
    else
    {
        page++;

        $('.btn-paging').removeClass('btn-info');
        $('.btn-paging-' + page).addClass('btn-info');

        $('.question').hide();
        $('.question-' + page).fadeIn();
    }

    return false;
});

$('.btn-submit').click(function(){
    $('#form-exercise').submit();

    return false;
});

$('.input-radio').click(function(){
    var page = $(this).attr('data-number');
    
    if (page == total)
    {
        alert('Kami rasa kamu sudah menjawab semua pertanyaan, jika sudah yakin silahkan klik tombol finish');
    }
});

// Hint Js
$('.btn-show-hint').click(function(){
    hint_id = $(this).attr('data-hint-id');
    question_id = $(this).attr('data-question-id');
    
    $.getJSON( base_url + 'hint/detail/' + hint_id + '/' + question_id, function( data ) {
        
        $('#hint_content').html(data.hint_content);
        
        // Save hint log id.
        hint_log_id = data.hint_log_id;
        
        $('#hintModal').modal('show');        
    });

    myTimer = setInterval(function(){ 
        timer++;
        
        $('#timer').html('<span class="label label-info">'+ formatSeconds(timer) +'</span>');
        
    }, 1000);

    return false;
});

$("#hintModal").on("hidden.bs.modal", function () {
    
    $.post( base_url + 'hint/close', { hint_log_id: hint_log_id, duration: timer })
    .done(function( data ) {
        console.log(data);
        $('#timer').html('<span class="label label-info">00:00:00</span>');
        timer = 0;
        clearInterval(myTimer);
    });

    return false;
});

/**
 * Timer js
 */
function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;
        
        if (--timer < 0) {
            alert('Waktu telah habis ..');
            $('#form-exercise').submit();
        }
    }, 1000);
}

window.onload = function () {
    startTimer(60 * duration, document.querySelector('#timer_duration'));
};

function formatSeconds(seconds)
{
    var date = new Date(1970,0,1);
    date.setSeconds(seconds);
    return date.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
}
</script>

<?php $this->load->view($this->theme . '/partials/footer.php');?>