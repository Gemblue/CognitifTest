<html>
<head>
    <title>Ujian Cognitif</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
    .container{
        padding-top:20px;
    }

    </style>
</head>

<body>
    <div class="container">

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                
                <form action="<?php echo site_url('room/answer');?>" method="post">
                    
                    <input type="hidden" name="page" value="<?php echo $page;?>">
                    <input type="hidden" name="question_id" value="<?php echo $question->question_id;?>">

                    <div class="well">
                        <p><?php echo $question->question_content;?></p>
                        
                        <?php
                        $hint = json_decode($question->hint);
                        
                        if (!empty($hint))
                        {
                            foreach ($hint  as $h)
                            {
                                $unique = str_replace(' ', '_', $h);

                                ?>
                                <div>
                                    <a href="#" class="btn-hint" id="<?php echo $unique;?>">Hint</a>
                                    <div style="margin-top:10px;" class="collapse <?php echo $unique;?>" style="display:none;">
                                        <div class="well"><?php echo $h;?></div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                        <br/>
                        <?php foreach ($options as $o) :?>
                            <div class="options">
                                <input type="radio" name="answer_id" value="<?php echo $o->option_id;?>"> <?php echo $o->option_content;?><br>
                            </div>
                        <?php endforeach;?>
                        <br/>
                        <button type="submit" class="btn btn-success">Jawab</button>
                    </div>
                </form>

            </div>
        </div>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <script>
    var open = true;
    $('.btn-hint').click(function(){
        
        var id = $(this).attr('id');
        
        if (open == true)
        {
            open = false;

            $('.collapse').hide();
            $('.' + id).show();
        }
        else
        {
            open = true;

            $('.collapse').hide();
            $('.' + id).hide();
        }
    });
    </script>

</body>
</html>