<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In</title>
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">
    <link href="<?php echo base_url('resources/theme/simple/plugins/bootstrap/css/bootstrap.css');?>" rel="stylesheet">
    <link href="https://bootswatch.com/3/lumen/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('resources/theme/simple/css/main.css');?>" rel="stylesheet">
</head>

<body>
    
    <div class="container">
        <header>
            <h3>Penelitian Lab Distance Learning & Digital Library dan Lab E-government & E-business Universitas Indonesia</h3>
            <hr/>
        </header>

        <content>
            <div class="row">
                <div class="col-md-6">
                    <p>
                        <h2>METAKOGNITIF: Kenali Cara Kamu Berfikir</h2>
                        Kemampuan untuk mengontrol ranah atau aspek kognitif. Meta kognitif mengendalikan enam tingkatan aspek kognitif yang didefinisikan oleh Benjamin Bloom dalan taksonomi Bloom yang terdiri dari tahapan ingatan, pemahaman, terapan, analisis dan sintetis, dan evalusi. Pada tahun 1991 taksonomi ini
                        direvisi oleh David Kratwahi menjadi mengingat, memahami, menerapkan, menganalisis, mengevaluasi, dan mencipta (creating)
                        
                        <div class="margin-md-top"><b>Sumber: Wikipedia</b></div>
                    </p>
                </div>
                <div class="col-md-6">
                    <h3>Masuk</h3>
                    <form class="form-login" method="POST" action="<?php echo site_url('permission/login');?>">
                        <h4>Selamat Datang di Sistem Meta Kognitif</h4>
                        <?php echo $this->session->flashdata('message');?>
                        <div class="form-group">
                            <label for="">Email *</label>
                            <input type="text" name="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Password *</label>
                            <input type="password" name="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Sekolah *</label>
                            <select name="school" class="form-control">
                                <option value="#" selected>- Pilih -</option>
                                <?php foreach($schools as $school) :?>
                                    <option value="<?php echo $school->id?>"><?php echo $school->name?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <button class="btn btn-info">Login</button>
                        
                        <div class="margin-md-top margin-md-bottom">Belum punya akun? silakan <a href="<?php echo site_url('permission/register')?>" class="mt-5">daftar</a></div>
                    </form>
                </div>
            </div>
        </content>

        <footer>
            <p>Penelitian Unggulan Perguruan Tinggi I 2018</p>
        </footer>

    </div>
    
    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('resources/theme/simple/plugins');?>/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('resources/theme/simple/plugins');?>/bootstrap/js/bootstrap.js"></script>
    
</body>

</html>