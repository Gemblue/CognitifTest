<?php $this->load->view($this->theme . '/partials/header.php');?>

<h3>Angket</h3>

<div class="margin-md-bottom">
    <span class="fa fa-info-circle"></span>&nbsp;&nbsp;&nbsp;Jawab dengan cara mengklik opsi yang tertera.
</div>

<form id="form-exercise" action="<?php echo site_url('room/finish')?>" method="post">
    
    <?php echo $this->session->flashdata('message');?>

    <div style="padding:50px;font-size:15px;" class="box">
        
        <ol>
        <?php $i=1; foreach ($questions as $question) :?>
            
            <li>
                <div><?php echo $question['question_content'];?></div>

                <div class="option">
                    <?php
                    $options = $this->Question_model->getOptionsByQuestion($question['question_id']);    
                    if (!empty($options))
                    {
                        ?>
                        
                        <?php foreach($options as $option) :?>
                            <div class="radio">
                                <label><input type="radio" class="input-radio" name="<?php echo $question['question_id']?>_answer" value="<?php echo $option->option_id?>" required><?php echo $option->option_content;?></label>
                            </div>
                        <?php endforeach;?>
                        
                        <?php
                    }
                    else
                    {
                        ?>Belum ada opsi jawaban ..<?php
                    }
                    ?>
                </div>
                
            </li>
        <?php $i++; endforeach;?>
        </ol>
        <hr />

        <div class="row">
            <div class="col-md-3">
                <h3>Angket :</h3>
            </div>
            <div class="col-md-6">
                <div class="alert alert-info">
                    Pada bagian ini tidak ada jawaban benar atau salah<br/>
                    Jawab setiap pertanyaan sesuai dengan pendapat kamu, tanpa perlu takut untuk salah menjawab<br/>
                    Klik tombol finish jika sudah selesai menjawab semua pertanyaan
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-success btn-answer"><span class="fa fa-check"></span> Finish</button>
            </div>
        </div>

    </div>

</form>

<?php $this->load->view($this->theme . '/partials/footer.php');?>