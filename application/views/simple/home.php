<?php $this->load->view($this->theme . '/partials/header.php');?>

<section>
    <h3>Daftar Uji Siswa</h3>

    <?php echo $this->session->flashdata('message');?>
    
    <?php if (!empty($groups)) :?>
        <div class="row">
            <?php foreach($groups as $group) :?>
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3><?php echo $group->title;?></h3>
                            <p><?php echo $group->subtitle;?></p>
                            <p>
                                <a href="<?php echo base_url('room/session/' . $group->id);?>" class="btn btn-success" role="button">Mulai</a> 
                                <a href="#" class="btn btn-info" role="button">Info</a>
                            </p>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    <?php else:?>
        Belum ada soal ..
    <?php endif;?>
    
</section>

<section>
    <h3>Kuisioner</h3>
    
    <div class="row">

        <?php foreach($surveys as $survey) :?>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3><?php echo $survey->title;?></h3>
                        <p><?php echo $survey->description;?></p>
                        <p><a href="<?php echo base_url('room/session/' . $survey->id);?>" class="btn btn-success" role="button">Mulai</a></p>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    </div>
    
</section>

<?php $this->load->view($this->theme . '/partials/footer.php');?>