<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Uji Meta Kognitif</title>
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">
    <link href="<?php echo base_url('resources/theme/simple/plugins/bootstrap/css/bootstrap.css');?>" rel="stylesheet">
    <link href="https://bootswatch.com/3/lumen/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('resources/theme/simple/css/main.css');?>" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
    .btn:hover{
        margin-top:0px;
    }
    </style>

    <script src="<?php echo base_url('resources/theme/simple/plugins');?>/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url('resources/theme/simple/plugins');?>/bootstrap/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
</head>

<body>

<input type="hidden" id="base_url" value="<?php echo base_url();?>" />

<div class="container">
    <header>
        <div class="row">
            <div class="col-md-8">
                <h2>Uji MetaKognitif 2018</h2>
            </div>
            <div class="col-md-4">
                <div class="profile pull-right">
                    <?php echo $this->session->name;?><br/>
                    <?php echo $this->session->nisn?>, <?php echo $this->session->school_name;?><br/>
                    <a href="#" onclick="return confirm('Fitur edit profil masih dalam pengembangan ..');" class="btn btn-success btn-xs">Edit Profil</a>
                    <a href="<?php echo site_url('permission/logout')?>"><span class="#"></span>&nbsp;&nbsp;Logout</a>
                </div>
            </div>
        </div>
        <hr/>
    </header>