<?php

/**
 * Ci auth
 *
 * Simple auth library for CI.
 * 
 * @author Ahmad Oriza
 */

class Ci_auth
{
    public $ci;
    public $school = 'cp_qa_school';
    public $user = 'cp_qa_users';
    public $role = 'cp_qa_user_roles';
    
    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->model('User_model');
    }

    /**
     * Login
     *
     * @param string $username
     * @param string $password
     * @return mixed
     */
    public function login($email, $password)
    {
        // Prepare
        $password = md5($password);
        
        // Detect di DB lokal
        $this->ci->db->select('*,' . $this->user . '.name as name,' . $this->user . '.id as user_id,' . $this->school . '.name as school_name');
        $this->ci->db->join($this->role, $this->role . '.id = '. $this->user . '.role_id');
        $this->ci->db->join($this->school, $this->school . '.id = '. $this->user . '.school_id');
        $this->ci->db->from($this->user);
        $this->ci->db->where($this->user . '.email', $email);
        $this->ci->db->where($this->user . '.password', $password);

        $result = $this->ci->db->get()->row_array();

        if (empty($result))
            return ['status' => 'failed', 'message' => '<div class="alert alert-danger">Username / password is not registered</div>'];
        
        if ($result['status'] == 'pending')
            return ['status' => 'failed', 'message' => '<div class="alert alert-info">Your account is not activated. Please check your email for activation / contact administrator</div>'];

        // Lalu generate session
        $this->ci->session->set_userdata([
            'logged_in' => true,
            'user_id' => $result['user_id'],
            'username' => $result['username'],
            'school_name' => $result['school_name'],
            'school_id' => $result['school_id'],
            'name' => $result['name'],
            'nisn' => $result['nisn'],
            'role_name' => $result['role_name'],
            'role_id' => $result['role_id'],
        ]);
        
        return ['status' => 'success', 'role_name' => $result['role_name'], 'message' => 'Welcome back!']; 
    }

    /**
     * Register
     *
     * @return bool
     */
    public function register($param, $status = 'pending', $role_id = 2, $send_email = false)
    {
        // Load helper
        $this->ci->load->helper('string');

        // Check username
		if ($this->ci->User_model->is_exist_username($param['username']))
		{
            return ['status' => 'failed', 'message' => 'Username is taken by other'];
        }

		// Check email
		if ($this->ci->User_model->is_exist_email($param['email']))
		{
			return ['status' => 'failed', 'message' => 'Email is taken by other'];
		}
        
		// Insert new user
		$user_id = $this->ci->User_model->insert([
            'name' => $param['name'],
            'username' => $param['username'],
            'email' => $param['email'],
            'nisn' => $param['nisn'],
            'status' => $status,
            'school_id' => $param['school_id'],
            'password' => $param['password'],
            'role_id' => $role_id
        ]);

        // Send email.
        if ($send_email)
        {
            $this->ci->load->library('email', [
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'ahmadoriza@gmail.com',
                'smtp_pass' => 'ingatM4T!',
                'mailtype'  => 'html', 
                'charset'   => 'iso-8859-1'
            ]);
            
            // Generate token.
            $token = md5($param['username'] . random_string('alnum', 5));
            $this->ci->User_model->update($user_id, ['token' => $token]);
            
            $confirm_link = site_url('permission/confirm/' . $token);
            $message = 'Hai! Welcome to CognitifTest.Com<br/><br/>Please verify your account by clicking this link <a href="'. $confirm_link .'">'. $confirm_link .'</a><br/><br/>Regards,<br/>CognitifTest.Com Team<br/><br/><a href="#">Unsubscribe</a>';
            
            $this->ci->email->set_newline("\r\n");
            $this->ci->email->from('ahmadoriza@gmail.com', 'Info CognitifTest');
            $this->ci->email->to($param['email']); 
            $this->ci->email->subject('Let verify your account - CognitifTest');
            $this->ci->email->message($message);
            
            if ($this->ci->email->send())
                return ['status' => 'success', 'message' => 'Successfully registered, check your email for activation.'];
            else
                return ['status' => 'failed', 'message' => 'Can not send email.'];
        }
        
		return ['status' => 'success', 'message' => 'Berhasil mendaftar, silahkan login dengan akun kamu.'];
    }

    /**
     * Confirm
     *
     * @return []
     */
    public function confirm($token)
    {
        // Is token exist?
        $this->ci->db->select('*');
        $this->ci->db->from($this->user);
        $this->ci->db->where($this->user . '.token', $token);
        $result = $this->ci->db->get()->row();
        
        if (empty($result))
        {    
            return ['status' => 'failed', 'message' => 'Token is not registered. Failed to activate account'];
        }
        else
        {
            $this->ci->User_model->update($result->id, ['token' => 'used', 'status' => 'active']);

            return ['status' => 'success', 'message' => 'Congratulation, your account is active!'];
        }
    }

    /**
     * Logout, destroy session
     *
     * @return bool
     */
    public function logout()
    {
        $this->ci->session->sess_destroy();
        return true;
    }

    /**
     * Is current user logged in?
     *
     * @return bool
     */
    public function logged_in()
    {
        $logged_in = $this->ci->session->userdata('logged_in');

        if ($logged_in)
            return true;
    }

    /**
     * Force Login
     *
     * Untuk meloginkan dan generate session orang langsung tanpa login. Modal id usernya saja.
     *
     * @param int $user_id
     * @return mixed
     */
    public function force_login($user_id)
    {
        $this->ci->db->select('*');
        $this->ci->db->join($this->departemen, $this->departemen . '.id_departemen = '. $this->user . '.id_departemen');
        $this->ci->db->join($this->user_role, $this->user_role . '.role_id = '. $this->user . '.role_id');
        $this->ci->db->join($this->cabang, $this->cabang . '.id_cabang = '. $this->user . '.id_cabang');
        $this->ci->db->from($this->user);
        $this->ci->db->where($this->user . '.user_id', $user_id);
        
        $result = $this->ci->db->get()->row_array();

        if (empty($result))
            return false;

        // Lalu generate session
        $this->ci->session->set_userdata([
            'logged_in' => true,
            'user_id' => $result['user_id'],
            'nip' => $result['nip'],
            'username' => $result['username'],
            'fullname' => $result['fullname'],
            'role_name' => $result['role_name'],
            'id_departemen' => $result['id_departemen'],
            'id_cabang' => $result['id_cabang'],
            'nama_cabang' => $result['nama_cabang'],
            'nama_departemen' => $result['nama_departemen'],
            'email' => $result['email'],
            'profile_picture' => $this->getProfilePicture($result['email'], 100)
        ]);
        
        return true;
    }

    /**
     * Checking current role.
     *
     * @param array $roles
     * @return bool
     */
    public function is_role_active($roles)
    {
        $current = $this->ci->session->userdata('role_name');

        if (in_array($current, $roles))
            return true; 
    }
    
    /**
     * Get current user active credential (logged in)
     *
     * @return array
     */
    public function getCurrentCredential()
    {
        $credential['fullname'] = $this->ci->session->userdata('logged_in');
        $credential['username'] = $this->ci->session->userdata('username');
        $credential['email'] = $this->ci->session->userdata('email');
        $credential['profile_picture'] = $this->ci->session->userdata('profile_picture');

        if (!empty($credential))
            return $credential;
    }

    /**
     * Get current user profile picture.
     *
     * @return string
     */
    public function getProfilePicture($email, $size = '100')
    {
        $picture = site_url('resources/img/avatar.png');
        
        return $picture;
    }

    /**
     * Get role id by name
     *
     * @return string
     */
    public function getIdByRoleName($role_name)
    {
        $this->ci->db->select('role_id');
        $this->ci->db->from($this->user_role);
        $this->ci->db->where('role_name', $role_name);
        $result = $this->ci->db->get()->row_array();

        if (!empty($result))
            return $result['role_id'];

        return null;
    }

    /**
     * Update Role
     */
    public function updateRole($user_id, $role_id)
    {
        $this->ci->db->where('user_id', $user_id);
        $this->ci->db->update($this->user, ['role_id' => $role_id]);
        
        return true;
    }
    
    /**
     * Change role
     *
     * @return string
     */
    public function changeRole($user_id, $role_name)
    {
        // Get role id
        $role_id = $this->getIdByRoleName($role_name);

        // Update role.
        $this->updateRole($user_id, $role_id);

        // Re generate session
        $this->force_login($user_id);

        return true;
    }
}