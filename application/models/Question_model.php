<?php

class Question_model extends CI_model
{
    protected $question = 'cp_qa_question';
    protected $group = 'cp_qa_group';
    protected $hint = 'cp_qa_hint';
    protected $question_rel = 'cp_qa_question_rel';
    protected $option = 'cp_qa_option';
    protected $result = 'cp_qa_result';
    protected $answer_log = 'cp_qa_answer_log';
    
    /**
     * Get detail question.
     *
     * @param int $question_id
     * @return object
     */
    public function getDetailQuestion($question_id)
    {
        $this->db->select('*');
        $this->db->from($this->question);
        $this->db->join($this->question_rel, $this->question_rel . '.question_id =' . $this->question . '.question_id');
        $this->db->join($this->group, $this->group . '.id =' . $this->question_rel . '.group_id');
        $this->db->where($this->question . '.question_id', $question_id);
        
        $result = $this->db->get()->row();
        
        return $result;
    }

    /**
     * Get questions by group.
     *
     * @return object
     */
    public function getQuestions($group_id = null, $keyword = null, $status = 'all', $label = null, $limit = null, $order = null)
    {
        $this->db->select('*,' . $this->question . '.question_id as id,' . $this->question . '.created_at as created_at,' . $this->question . '.updated_at as updated_at');
        $this->db->from($this->question_rel);
        $this->db->join($this->question, $this->question_rel . '.question_id = ' . $this->question . '.question_id');
        $this->db->join($this->group, $this->group . '.id = ' . $this->question_rel . '.group_id');
        
        if ($status == 'all')
            $this->db->where_in($this->question . '.question_status', ['publish', 'draft']);
        else
            $this->db->where($this->question . '.question_status', $status);
        
        if ($group_id != null)
            $this->db->where($this->question_rel . '.group_id', $group_id);
        
        if ($keyword != null)
            $this->db->like($this->question . '.question_title', $keyword);

        if ($label != null)
            $this->db->where($this->group . '.label', $label);

        $this->db->order_by($this->question . '.created_at', 'desc');
        
        if ($limit != null)
        {
            $this->db->limit($limit, $order);
        }
        
        return $this->db->get()->result_array();
    }

    /**
     * Get question group.
     *
     * @param int $question_id
     * @return object
     */
    public function getQuestionGroups($question_id)
    {
        $this->db->select($this->group . '.*');
        $this->db->from($this->question_rel);
        $this->db->join($this->group, $this->group . '.id =' . $this->question_rel . '.group_id');
        $this->db->where($this->question_rel . '.question_id', $question_id);
        
        $groups = $this->db->get()->result_array();

        if (!empty($groups))
        {
            $final = null;
            
            foreach($groups as $g) {
                $final .= $g['title'] . ', ';
            }

            return rtrim($final, ', ');
        }

        return null;
    }

    /**
     * Get total questions by group.
     *
     * @param int $group_id
     * @param string $status
     * @return object
     */
    public function getTotalQuestions($group_id = null, $keyword = null, $status = 'all', $label = null)
    {
        $this->db->select($this->question_rel . '.id');
        $this->db->from($this->question_rel);
        $this->db->join($this->question, $this->question_rel . '.question_id = ' . $this->question . '.question_id');
        $this->db->join($this->group, $this->group . '.id = ' . $this->question_rel . '.group_id');
        
        if ($status == 'all')
            $this->db->where_in($this->question . '.question_status', ['publish', 'draft']);
        else
            $this->db->where($this->question . '.question_status', $status);
        
        if ($group_id != null)
            $this->db->where($this->question_rel . '.group_id', $group_id);
        
        if ($keyword != null)
            $this->db->like($this->question . '.question_title', $keyword);
        
        if ($label != null)
            $this->db->where($this->group . '.label', $label);

        return $this->db->get()->num_rows();
    }

    /**
     * Get option by question
     *
     * @param int $question_id
     * @param int $limit
     * @return object
     */
    public function getOptionsByQuestion($question_id, $limit = 10)
    {
        $this->db->select('*');
        $this->db->from($this->option);
        $this->db->where($this->option . '.question_id', $question_id);
        $this->db->order_by($this->option . '.created_at', 'desc');
        
        return $this->db->get()->result();   
    }

    /**
     * Get hints by question
     *
     * @param int $question_id
     * @param int $limit
     * @return object
     */
    public function getHintsByQuestion($question_id, $limit = 10)
    {
        $this->db->select('*');
        $this->db->from($this->hint);
        $this->db->where($this->hint . '.question_id', $question_id);
        $this->db->order_by($this->hint . '.created_at', 'desc');
        
        return $this->db->get()->result();   
    }

    /**
     * Get answer logs.
     *
     * @return array
     */
    public function getAnswerLogs($session_id)
    {
        $this->db->select('*');
        $this->db->from($this->answer_log);
        $this->db->join($this->question, $this->question . '.question_id = ' . $this->answer_log . '.question_id');
        $this->db->join($this->option, $this->option . '.option_id = ' . $this->answer_log . '.answer');
        $this->db->where($this->answer_log . '.session_id', $session_id);  
        
        return $this->db->get()->result();
    }

    /**
     * Get total wrong/right answer
     *
     * @return int
     */
    public function getTotalLog($session_id, $status)
    {
        $this->db->select('*');
        $this->db->from($this->answer_log);
        $this->db->where($this->answer_log . '.log_status', $status);
        $this->db->where($this->answer_log . '.session_id', $session_id);
        
        return $this->db->get()->num_rows();   
    }

    /**
     * Is correct
     *
     * @param int $question_id
     * @param int $answer_id
     * @return bool
     */
    public function isCorrect($question_id, $answer_id)
    {
        $this->db->select('*');
        $this->db->from($this->option);
        $this->db->where($this->option . '.question_id', $question_id);
        $this->db->where($this->option . '.option_id', $answer_id);
        $this->db->where($this->option . '.is_right', true);

        $result = $this->db->get()->row();
        
        if (empty($result))
            return false;
        
        return true;
    }

    /**
     * Is ready, is question ready
     *
     * @param int $group_id
     * @param int $category_id
     * @return bool
     */
    public function isReady($group_id, $category_id)
    {
        $result = DB::table($this->question_rel)
                ->select('*')
                ->where($this->question_rel . '.group_id', $group_id)
                ->where($this->question_rel . '.category_id', $category_id)
                ->first();
        
        if (!empty($result))
            return true;
        else
            return false;
    }

    /**
     * Is completed
     *
     * @param int $user_id
     * @param int $group_id
     * @param int $category_id
     * @return bool
     */
    public function isCompleted($user_id, $group_id, $category_id)
    {
        $result = DB::table($this->result)
                ->select('*')
                ->where($this->result . '.user_id', $user_id)
                ->where($this->result . '.group_id', $group_id)
                ->where($this->result . '.category_id', $category_id)
                ->where($this->result . '.score', '>=', 70)
                ->first();
        
        if (!empty($result))
            return true;
        else
            return false;
    }

    /**
     * To answer the question
     *
     * @param int $session_id
     * @param int $question_id
     * @param int $answer_id
     * @return bool
     */
    public function answer($session_id, $question_id, $answer_id)
    {
        // Is correct?
        $is_correct = $this->Question_model->isCorrect($question_id, $answer_id);
        
        $this->db->insert($this->answer_log, [
            'session_id' => $session_id,
            'question_id' => $question_id,
            'answer' => $answer_id,
            'log_status' => $is_correct,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        
        $id = $this->db->insert_id();
        
        return $id;  
    }

    /**
     * Generate session.
     *
     * @return bool
     */
    public function generateSession($group_id, $user_id)
    {
        $this->db->insert($this->result, [
            'group_id' => $group_id,
            'user_id' => $user_id,
            'total_question' => 0,
            'total_right'=> 0,
            'total_wrong' => 0,
            'score' => 0,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        
        return $this->db->insert_id();
    }

    /**
     * Save result.
     *
     * @return []
     */
    public function saveResult($session_id, $group_id, $duration)
    {
        // Get total question ..
        $total_question = $this->getTotalQuestions($group_id, null, 'publish');
        
        // Get right answer ..
        $total_right = $this->getTotalLog($session_id, true);
        $total_wrong = $this->getTotalLog($session_id, false);
        $score = ($total_right / $total_question) * 100;
        $score = intval($score);

        $date = date('Y-m-d H:i:s');
        
        $this->db->where('result_id', $session_id);
        $this->db->update($this->result, [
            'score' => $score,
            'total_right' => $total_right,
            'total_wrong' => $total_wrong,
            'total_question' => $total_question,
            'duration' => $duration,
            'updated_at' => $date
        ]);
        
        return ['status' => 'success', 'score' => $score];
    }

    /**
     * Import Quiz
     *
     * @param array $questions
     * @return bool
     */
    public function import($group_id, $category_id, $questions)
    {
        $option_final = [];

        foreach ($questions as $q)
        {
            // Insert the question
            foreach($q['pilihan'] as $number => $value)
            {
                $option_final[] = [
                    'option_content' => $value,
                    'is_right' => ($number == $q['jawaban']) ? 1 : 0
                ];
            }
            
            $question = [
                'question_content' => $q['soal'],
                'creator' => 1,
                'question_status' => 'publish'
            ];  
            
            $this->insertQuestion($group_id, $category_id, $question, $option_final);
            
            $option_final = null;
        }
        
        return true;
    }

    /**
     * Insert
     *
     * @return bool
     */
    public function insertQuestion($group_id, $question)
    {
        $date = date('Y-m-d H:i:s');

        unset($question['group_id']);
        
        $question['created_at'] = $date;
        $question['creator'] = 1;
        $question['question_status'] = 'publish';
        
        $this->db->insert($this->question, $question);
        $question_id = $this->db->insert_id();
        
        // Insert to group relation
        $param__['created_at'] = $date;
        $param__['group_id'] = $group_id;
        $param__['question_id'] = $question_id;
        
        $this->db->insert($this->question_rel, $param__);

        return ['status' => 'success', 'id' => $question_id];
    }

    /**
     * Update Question
     *
     * @return bool
     */
    public function updateQuestion($question)
    {
        $question['updated_at'] = date('Y-m-d H:i:s');
        
        // Update relation
        $this->db->delete($this->question_rel, ['question_id' => $question['question_id']]);
        $this->db->insert($this->question_rel, [
            'created_at' => $question['updated_at'],
            'group_id' => $question['group_id'],
            'question_id' => $question['question_id']
        ]);
        
        unset($question['group_id']);
        
        return $this->db->update($this->question, $question, ['question_id' => $question['question_id']]);
    }

    /**
     * Remove
     *
     * @return bool
     */
    public function removeQuestion($question_id)
    {
        DB::table($this->question)->where('question_id', $question_id)->update([
            'question_status' => 'deleted'
        ]);
        
        return true;
    }
}