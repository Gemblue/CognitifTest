<?php

class Stats_model extends CI_model
{
    protected $group = 'cp_qa_group';
    protected $result = 'cp_qa_result';
    protected $question = 'cp_qa_question';
    protected $school = 'cp_qa_school';
    protected $users = 'cp_qa_users';
    
    /**
     * Get total users.
     *
     * @return object
     */
    public function getTotalUsers()
    {
        $this->db->select('id');
        $this->db->from($this->users);
        
        return $this->db->get()->num_rows();
    }

    /**
     * Get total result.
     *
     * @return object
     */
    public function getTotalResult()
    {
        $this->db->select('result_id');
        $this->db->from($this->result);
        
        return $this->db->get()->num_rows();
    }

    /**
     * Get total school.
     *
     * @return object
     */
    public function getTotalSchool()
    {
        $this->db->select('id');
        $this->db->from($this->school);
        
        return $this->db->get()->num_rows();
    }

    /**
     * Get total group
     *
     * @return object
     */
    public function getTotalGroup()
    {
        $this->db->select('id');
        $this->db->from($this->group);
        
        return $this->db->get()->num_rows();
    }

    /**
     * Get total questions.
     *
     * @return object
     */
    public function getTotalQuestions()
    {
        $this->db->select('question_id');
        $this->db->from($this->question);
        
        return $this->db->get()->num_rows();
    }
}