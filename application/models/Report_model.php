<?php

class Report_model extends CI_model
{
    protected $result = 'cp_qa_result';
    protected $school = 'cp_qa_school';
    protected $user = 'cp_qa_users';
    protected $group = 'cp_qa_group';
    
    /**
     * Get all results.
     *
     * @return object
     */
    public function getResults($filters = [], $limit = null, $order = null)
    {
        $this->db->select('*,' . $this->school . '.name as school_name,' . $this->user . '.name as name,' . $this->result . '.created_at as created_at');
        $this->db->from($this->result);
        $this->db->join($this->user, $this->user . '.id =' . $this->result . '.user_id');
        $this->db->join($this->school, $this->school . '.id =' . $this->user . '.school_id');
        $this->db->join($this->group, $this->group . '.id =' . $this->result . '.group_id');
        
        if ($filters['name'] != '')
            $this->db->where($this->user . '.name', $filters['name']);

        if ($filters['nisn'] != '')
            $this->db->where($this->user . '.nisn', $filters['nisn']);

        if ($filters['school_id'] != '')
            $this->db->where($this->user . '.school_id', $filters['school_id']);

        if ($filters['group_id'] != '')
            $this->db->where($this->result . '.group_id', $filters['group_id']);

        if ($filters['label'] != '')
            $this->db->where($this->group . '.label', $filters['label']);
        
        $this->db->order_by($this->result . '.created_at', 'desc');
        
        if ($limit != null)
            $this->db->limit($limit, $order);
        
        return $this->db->get()->result();
    }

    /**
     * Get detail result.
     *
     * @return object
     */
    public function getResult($id)
    {
        $this->db->select('*, ' . $this->user . '.name as name,' . $this->result . '.duration as result_duration,' . $this->school . '.name as school_name');
        $this->db->from($this->result);
        $this->db->join($this->user, $this->user . '.id =' . $this->result . '.user_id');
        $this->db->join($this->school, $this->school . '.id =' . $this->user . '.school_id');
        $this->db->join($this->group, $this->group . '.id =' . $this->result . '.group_id');
        $this->db->where('result_id', $id);
        
        return $this->db->get()->row();
    }

    /**
     * Get total results
     *
     * @return object
     */
    public function getTotalResults($filters = [])
    {
        $this->db->select('*');
        $this->db->from($this->result);
        $this->db->join($this->user, $this->user . '.id =' . $this->result . '.user_id');
        $this->db->join($this->school, $this->school . '.id =' . $this->user . '.school_id');
        $this->db->join($this->group, $this->group . '.id =' . $this->result . '.group_id');

        if ($filters['name'] != '')
            $this->db->where($this->user . '.name', $filters['name']);

        if ($filters['nisn'] != '')
            $this->db->where($this->user . '.nisn', $filters['nisn']);

        if ($filters['school_id'] != '')
            $this->db->where($this->user . '.school_id', $filters['school_id']);

        if ($filters['group_id'] != '')
            $this->db->where($this->result . '.group_id', $filters['group_id']);

        if ($filters['label'] != '')
            $this->db->where($this->group . '.label', $filters['label']);
        
        return $this->db->get()->num_rows();
    }

    /**
     * Get results with hint
     *
     * @return object
     */
    public function getResultsWithHint()
    {
        $final = [];

        $this->db->select($this->result . '.result_id,'.$this->user . '.name,'.$this->school.'.name as school_name,'.$this->group.'.title');
        $this->db->from($this->result);
        $this->db->join($this->user, $this->user . '.id =' . $this->result . '.user_id');
        $this->db->join($this->school, $this->school . '.id =' . $this->user . '.school_id');
        $this->db->join($this->group, $this->group . '.id =' . $this->result . '.group_id');

        $this->db->order_by($this->result . '.created_at', 'desc');
        
        $results = $this->db->get()->result_array();

        $i = 0;

        foreach ($results as $result)
        {
            $logs = $this->Question_model->getAnswerLogs($result['result_id']);
            
            if (!empty($logs))
            {
                $final[$i]['nama_siswa'] = $result['name']; 
                $final[$i]['sekolah'] = $result['school_name']; 
                $final[$i]['kelompok_soal'] = $result['title']; 
                $final[$i]['log'] = [];

                $j = 0;

                foreach ($logs as $log)
                {
                    $final[$i]['log'][$j]['soal'] = $log->question_title; 
                    $final[$i]['log'][$j]['akses_hint'] = $this->getTotalHintLog($result['result_id'], $log->question_id); 
                    $final[$i]['log'][$j]['hint_prosedural'] = $this->getTotalHintDuration($result['result_id'], $log->question_id, 'Prosedural'); 
                    $final[$i]['log'][$j]['hint_strategi'] = $this->getTotalHintDuration($result['result_id'], $log->question_id, 'Strategi'); 
                    $final[$i]['log'][$j]['hint_deklaratif'] = $this->getTotalHintDuration($result['result_id'], $log->question_id, 'Deklaratif'); 
                    
                    $j++;
                }

                $i++;
            }
        }

        return $final;
    }

    public function getTotalHintLog($result_id, $question_id, $category = null)
    {
        $this->db->select('id');
        $this->db->from('cp_qa_hint_log');
        $this->db->join('cp_qa_hint', 'cp_qa_hint.hint_id = cp_qa_hint_log.hint_id');
        $this->db->where('cp_qa_hint_log.result_id', $result_id);
        
        if ($category != null)
            $this->db->where('cp_qa_hint.hint_category', $category);
        
        $total = $this->db->get()->num_rows();
        
        if ($total)
            return $total . ' Kali';

        return 'Nihil';
    }

    public function getTotalHintDuration($result_id, $question_id, $category = null)
    {
        $this->db->select('sum(hint_log_duration) as duration');
        $this->db->from('cp_qa_hint_log');
        $this->db->join('cp_qa_hint', 'cp_qa_hint.hint_id = cp_qa_hint_log.hint_id');
        $this->db->where('cp_qa_hint_log.result_id', $result_id);
        
        if ($category != null)
            $this->db->where('cp_qa_hint.hint_category', $category);
        
        $result = $this->db->get()->row();
        
        if (isset($result->duration))
            return $result->duration . ' Detik';

        return '0 Detik';
    }
}
