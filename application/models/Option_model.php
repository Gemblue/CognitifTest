<?php

class Option_model extends CI_model
{
    protected $option = 'cp_qa_option';
    protected $question = 'cp_qa_question';
    
    /**
     * Get options
     *
     * @return object
     */
    public function getOptions($status = 'all', $limit = 5, $order = 0)
    {
        $this->db->select('*');
        $this->db->from($this->option);
        
        if ($status == 'all')
            $this->db->where_in('status', ['publish', 'draft']);
        else
            $this->db->where('status', $status);
        
        $this->db->order_by('created_at', 'desc');
        
        if ($limit != null)
        {
            $this->db->limit($limit, $order);
        } 
        
        return $this->db->get()->result();
    }
    
    /**
     * Get total group
     *
     * @return object
     */
    public function getTotalOptions($status = 'all')
    {
        $this->db->select('*');
        $this->db->from($this->option);
        
        if ($status == 'all')
            $this->db->where_in('status', ['publish', 'draft']);
        else
            $this->db->where('status', $status);
        
        return $this->db->get()->num_rows();
    }

    /**
     * Get detail
     *
     * @param int $id
     * @return object
     */
    public function getDetail($id)
    {
        $this->db->select('*');
        $this->db->from($this->option);
        $this->db->where('option_id', $id);
        
        return $this->db->get()->row_array();
    }

    /**
     * Insert.
     *
     * @return bool
     */
    public function insert($param)
    {
        $param['created_at'] = date('Y-m-d H:i:s');
        
        return $this->db->insert($this->option, $param);
    }

    /**
     * Update.
     *
     * @return bool
     */
    public function update($param)
    {
        $param['updated_at'] = date('Y-m-d H:i:s');
        
        return $this->db->update($this->option, $param, ['option_id' => $param['option_id']]);
    }

    /**
     * Publish
     *
     * @return bool
     */
    public function publish($id)
    {
        if ($this->db->update($this->option, ['status' => 'draft']))
        {
            $this->db->where('id', $id);
            $this->db->update($this->option, ['status' => 'publish']);
        }
        
        return true;
    }

    /**
     * Draft
     *
     * @return bool
     */
    public function draft($id)
    {
        $this->db->where('id', $id);
        $this->db->update($this->option, ['status' => 'draft']);
        
        return true;
    }
}