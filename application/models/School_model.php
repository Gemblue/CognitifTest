<?php

class School_model extends CI_model
{
    protected $school = 'cp_qa_school';
    protected $user = 'cp_qa_users';
    
    /**
     * Get all school
     *
     * @return object
     */
    public function getSchools($status = 'all', $keyword = null, $limit = 5, $order = 0)
    {
        $this->db->select('*');
        $this->db->from($this->school);
        
        if ($status == 'all')
            $this->db->where_in('status', ['active', 'inactive']);
        else
            $this->db->where('status', $status);
        
        if ($keyword != null)
            $this->db->like('name', $keyword);

        $this->db->order_by('created_at', 'desc');
        
        if ($limit != null)
        {
            $this->db->limit($limit, $order);
        } 
        
        return $this->db->get()->result();
    }

    /**
     * Get total schools
     *
     * @return object
     */
    public function getTotalSchools($status = 'all', $keyword = null)
    {
        $this->db->select('*');
        $this->db->from($this->school);
        
        if ($status == 'all')
            $this->db->where_in('status', ['publish', 'draft']);
        else
            $this->db->where('status', $status);
        
        if ($keyword != null)
            $this->db->like('name', $keyword);
        
        return $this->db->get()->num_rows();
    }

    /**
     * Get total students
     *
     * @return object
     */
    public function getTotalStudents($school_id)
    {
        $this->db->select('*');
        $this->db->from($this->user);
        $this->db->where('status', 'active');
        $this->db->where('school_id', $school_id);

        return $this->db->get()->num_rows();
    }

    /**
     * Get detail
     *
     * @param int $id
     * @return object
     */
    public function getDetail($id)
    {
        $this->db->select('*');
        $this->db->from($this->school);
        $this->db->where('id', $id);
        
        return $this->db->get()->row_array();
    }

    /**
     * Insert.
     *
     * @return bool
     */
    public function insert($param)
    {
        $param['created_at'] = date('Y-m-d H:i:s');
        
        return $this->db->insert($this->school, $param);
    }

    /**
     * Update.
     *
     * @return bool
     */
    public function update($param)
    {
        $param['updated_at'] = date('Y-m-d H:i:s');
        
        return $this->db->update($this->school, $param, ['id' => $param['id']]);
    }

    /**
     * Publish
     *
     * @return bool
     */
    public function publish($id)
    {
        if ($this->db->update($this->school, ['status' => 'draft']))
        {
            $this->db->where('id', $id);
            $this->db->update($this->school, ['status' => 'publish']);
        }
        
        return true;
    }

    /**
     * Draft
     *
     * @return bool
     */
    public function draft($id)
    {
        $this->db->where('id', $id);
        $this->db->update($this->school, ['status' => 'draft']);
        
        return true;
    }
}