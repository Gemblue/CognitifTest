<?php

class Hint_model extends CI_model
{
    protected $hint = 'cp_qa_hint';
    protected $hint_log = 'cp_qa_hint_log';
    
    /**
     * Get detail
     *
     * @param int $id
     * @return object
     */
    public function getDetail($id)
    {
        $this->db->select('*');
        $this->db->from($this->hint);
        $this->db->where('hint_id', $id);
        
        return $this->db->get()->row_array();
    }

     /**
     * Get hint logs
     *
     * @return object
     */
    public function getHintLogs($result_id, $question_id)
    {
        $this->db->select('*');
        $this->db->from($this->hint_log);
        $this->db->join($this->hint, $this->hint . '.hint_id =' . $this->hint_log . '.hint_id');
        $this->db->where($this->hint_log . '.result_id', $result_id);
        $this->db->where($this->hint_log . '.question_id', $question_id);
        
        return $this->db->get()->result_array();
    }

    /**
     * Get total hint log
     *
     * @return object
     */
    public function getTotalHintLog($session_id, $question_id)
    {
        $this->db->select('*');
        $this->db->from($this->hint_log);
        $this->db->where('result_id', $session_id);
        $this->db->where('question_id', $question_id);
        
        return $this->db->get()->num_rows();
    }

    /**
     * Get total duration hint log
     *
     * @return object
     */
    public function getTotalDurationHintLog($session_id = null, $question_id = null)
    {
        $this->db->select('sum(hint_log_duration) as total');
        $this->db->from($this->hint_log);
        
        if ($session_id != null)
            $this->db->where('result_id', $session_id);

        if ($question_id != null)
            $this->db->where('question_id', $question_id);
        
        $result = $this->db->get()->row();
        
        if ($result->total > 0)
            return $result->total;
        
        return '0';
    }
    
    /**
     * Save Log.
     *
     * @return bool
     */
    public function saveLog($param)
    {
        $param['created_at'] = date('Y-m-d H:i:s');
        
        if ($this->db->insert($this->hint_log, $param))
            return $this->db->insert_id();

        return false;
    }

    /**
     * Insert.
     *
     * @return bool
     */
    public function insert($param)
    {
        $param['created_at'] = date('Y-m-d H:i:s');
        
        return $this->db->insert($this->hint, $param);
    }

    /**
     * Update Log
     *
     * @return bool
     */
    public function updateLog($hint_log_id, $duration)
    {
        return $this->db->update($this->hint_log, ['hint_log_duration' => $duration], ['id' => $hint_log_id]);
    }

    /**
     * Update.
     *
     * @return bool
     */
    public function update($param)
    {
        $param['updated_at'] = date('Y-m-d H:i:s');
        
        return $this->db->update($this->hint, $param, ['hint_id' => $param['hint_id']]);
    }
}