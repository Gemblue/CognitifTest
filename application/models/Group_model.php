<?php

class Group_model extends CI_model
{
    protected $group = 'cp_qa_group';
    protected $group_rel = 'cp_qa_group_rel';
    protected $school = 'cp_qa_school';
    protected $question = 'cp_qa_question';
    
    /**
     * Get all groups
     *
     * @return object
     */
    public function getGroups($status = 'all', $keyword = null, $label = null, $limit = null, $order = null)
    {
        $this->db->select('*,' . $this->group . '.id as id,' . $this->group . '.created_at as created_at');
        $this->db->from($this->group);
        
        if ($status == 'all')
            $this->db->where_in('status', ['publish', 'draft']);
        else
            $this->db->where('status', $status);
        
        if ($keyword != null)
            $this->db->like('title', $keyword);
        
        if ($label != null)
            $this->db->where('label', $label);
        
        $this->db->order_by('created_at', 'desc');
        
        if ($limit != null)
        {
            $this->db->limit($limit, $order);
        } 
        
        return $this->db->get()->result();
    }

    /**
     * Get group by school id.
     *
     * @return object
     */
    public function getGroupsBySchool($status = 'all', $keyword = null, $label = null, $school, $limit = null, $order = null)
    {
        $this->db->select('*,' . $this->group . '.id as id,' . $this->group . '.created_at as created_at');
        $this->db->from($this->group);
        $this->db->join($this->group_rel, $this->group_rel . '.group_id = ' . $this->group . '.id');
        
        if ($status == 'all')
            $this->db->where_in('status', ['publish', 'draft']);
        else
            $this->db->where('status', $status);
        
        if ($keyword != null)
            $this->db->like('title', $keyword);
        
        if ($label != null)
            $this->db->where('label', $label);
        
        $this->db->where($this->group_rel . '.school_id', $school);

        $this->db->order_by($this->group . '.created_at', 'desc');
        
        if ($limit != null)
        {
            $this->db->limit($limit, $order);
        } 
        
        return $this->db->get()->result();
    }
    
    /**
     * Get total group
     *
     * @return object
     */
    public function getTotalGroups($status = 'all', $keyword = null, $label = null)
    {
        $this->db->select('*');
        $this->db->from($this->group);
        
        if ($status == 'all')
            $this->db->where_in('status', ['publish', 'draft']);
        else
            $this->db->where('status', $status);

        if ($keyword != null)
            $this->db->like('title', $keyword);

        if ($label != null)
            $this->db->where('label', $label);
        
        return $this->db->get()->num_rows();
    }

    /**
     * Get detail
     *
     * @param int $id
     * @return object
     */
    public function getDetail($id)
    {
        $this->db->select('*');
        $this->db->from($this->group);
        $this->db->where('id', $id);
        
        return $this->db->get()->row_array();
    }

    /**
     * Insert.
     *
     * @return bool
     */
    public function insert($param)
    {
        $param['created_at'] = date('Y-m-d H:i:s');
        
        return $this->db->insert($this->group, $param);
    }

    /**
     * Connected.
     *
     * @return bool
     */
    public function getConnected($group_id)
    {
        $this->db->select('*');
        $this->db->from($this->group_rel);
        $this->db->join($this->school, $this->school . '.id = ' . $this->group_rel . '.school_id');
        $this->db->where($this->group_rel . '.group_id', $group_id);

        return $this->db->get()->result_array();
    }

    /**
     * Remove Connected.
     *
     * @return bool
     */
    public function removeConnected($group_id, $school_id)
    {
        $this->db->delete($this->group_rel, ['group_id' => $group_id, 'school_id' => $school_id]);
        
        return ['status' => 'success', 'message' => 'Berhasil menghapus ..'];
    }

    /**
     * Share.
     *
     * @return bool
     */
    public function share($group_id, $school_id)
    {
        // Is connected before?
        $this->db->select('*');
        $this->db->from($this->group_rel);
        $this->db->where('school_id', $school_id);
        $this->db->where('group_id', $group_id);
        
        $result = $this->db->get()->result();

        if (!empty($result)) {
            return ['status' => 'failed', 'message' => 'Sekolah tersebut sudah terhubung sebelumnya.'];
        } else {
            
            $this->db->insert($this->group_rel, [
                'group_id' => $group_id,
                'school_id' => $school_id,
                'created_at' => date('Y-m-d H:i:s')
            ]);
            
            return ['status' => 'success', 'message' => 'Berhasil menghubungkan ..'];
        }
    }

    /**
     * Update.
     *
     * @return bool
     */
    public function update($param)
    {
        $param['updated_at'] = date('Y-m-d H:i:s');
        
        return $this->db->update($this->group, $param, ['id' => $param['id']]);
    }

    /**
     * Publish
     *
     * @return bool
     */
    public function publish($id)
    {
        if ($this->db->update($this->group, ['status' => 'draft']))
        {
            $this->db->where('id', $id);
            $this->db->update($this->group, ['status' => 'publish']);
        }
        
        return true;
    }

    /**
     * Draft
     *
     * @return bool
     */
    public function draft($id)
    {
        $this->db->where('id', $id);
        $this->db->update($this->group, ['status' => 'draft']);
        
        return true;
    }
}