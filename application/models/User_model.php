<?php

class User_model extends CI_Model
{
	protected $users = 'cp_qa_users';
	protected $school = 'cp_qa_school';
	protected $role = 'cp_qa_user_roles';
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_all_users($result = 'data', $keyword = null, $status = 'all', $limit = 10, $limit_order = 1)
	{
		if ($result == 'total')
			$this->db->select($this->users . '.id');
		else
			$this->db->select('*,' . $this->users . '.id as id,' . $this->users . '.status as status,' . $this->users . '.created_at as created_at,' . $this->users . '.name as name,' . $this->school . '.name as school_name');
		
		$this->db->from($this->users);
		$this->db->join($this->school, $this->school . '.id = ' . $this->users . '.school_id');
        
		if (!empty($status) && $status != 'all')
		{
			$this->db->where($this->users.'.status', $status);
        }
        
        if ($keyword != null)
            $this->db->like($this->users . '.name', $keyword);

		$this->db->where($this->users.'.status !=', 'deleted');
		
		if ($result == 'total')
			return $this->db->get()->num_rows();

		$this->db->order_by($this->users.'.id', 'desc');
		$this->db->limit($limit, $limit_order);
		
		return $this->db->get()->result();
    }
    
    public function get_all_roles()
	{
        $this->db->select('*');
        $this->db->from($this->role);

        return $this->db->get()->result();
    }

	public function search_users($result = 'data', $status = 'all', $keyword = null, $limit = 10, $limit_order = 1)
	{
		if ($result == 'total')
			$this->db->select($this->users . '.id');
		else
			$this->db->select('*,' . $this->users . '.id as id,' . $this->users . '.status as status,' . $this->users . '.created_at as created_at,' . $this->users . '.name as name,' . $this->school . '.name as school_name');
		
		$this->db->from($this->users);
		
		$this->db->like($this->users . '.name', $keyword);

		if (!empty($status) && $status != 'all')
		{
			$this->db->where($this->users.'.status', $status);
		}

		$this->db->where($this->users.'.status !=', 'deleted');
		
		if ($result == 'total')
			return $this->db->get()->num_rows();

		$this->db->order_by($this->users.'.id', 'desc');
		$this->db->limit($limit, $limit_order);
		
		return $this->db->get()->result();
	}

	public function get_detail($id)
	{
		$this->db->select('*,' . $this->users . '.id as id,' . $this->role . '.id as role_id');
        $this->db->from($this->users);
        $this->db->join($this->role, $this->role . '.id =' . $this->users . '.role_id');
		$this->db->where($this->users . '.id', $id);
        
		return $this->db->get()->row();
    }
    
    public function get_detail_by_username($username)
	{
		$this->db->select('*');
		$this->db->from($this->users);
		$this->db->where('username', $username);
        
		return $this->db->get()->row();
	}

	public function get_user_id($field_value, $by_field)
	{
		$this->db->select('id');
		$this->db->from($this->users);
		$this->db->where($by_field, $field_value);

		$result = $this->db->get()->row();

		if (!empty($result))
			return $result->id;
		
		return null;
	}

	public function get_username($field_value, $by_field)
	{
		$this->db->select('username');
		$this->db->from($this->users);
		$this->db->where($by_field, $field_value);

		$result = $this->db->get()->row();

		if (!empty($result))
			return $result->username;
		
		return null;
	}

	public function get_user_mail($field_value, $by_field)
	{
		$this->db->select('email');
		$this->db->from($this->users);
		$this->db->where($by_field, $field_value);

		$result = $this->db->get()->row();

		if (!empty($result))
			return $result->email;
		
		return null;
	}

	public function get_field_value($field_name, $by_field, $field_value)
	{
		$this->db->select($field_name . ' as field');
		$this->db->from($this->users);
		$this->db->where($by_field, $field_value);

		$result = $this->db->get()->row();

		if (!empty($result))
			return $result->field;
		
		return null;
	}

	public function get_avatar($user_id, $size = 'md')
	{
		return 'http://www.gravatar.com/avatar/'.md5(strtolower(trim( $this->User_model->get_user_mail($user_id, 'id'))));
	}

	public function is_exist_username($username)
	{
		$this->db->select('id');
		$this->db->from($this->users);
		$this->db->where('username', $username);

		if ($this->db->get()->num_rows() > 0)
			return true;

		return false;
	}

	public function is_exist_password($user_id, $password)
	{
		$this->db->select('id');
		$this->db->from($this->users);
		$this->db->where('password', $password);
		$this->db->where('id', $user_id);

		if ($this->db->get()->num_rows() > 0)
			return true;

		return false;
	}

	public function is_exist_email($email)
	{
		$this->db->select('id');
		$this->db->from($this->users);
		$this->db->where('email', $email);

		if ($this->db->get()->num_rows() > 0)
			return true;

		return false;
	}

	public function update($user_id, $param)
	{
		$this->db->update($this->users, $param, ['id' => $user_id]);
		
		return true;
	}

	public function insert($param)
	{
		$param['created_at'] = date('Y-m-d H:i:s');

        // Password mutate.
        $param['password'] = md5($param['password']);
        
		$this->db->insert($this->users, $param);

		return $this->db->insert_id();
	}

	public function delete($id)
	{
		$this->db->update($this->users, [
			'status' => 'deleted'
		], ['id' => $id]);
		
		return true;
	}

	public function activate($id)
	{
		$this->db->update($this->users, ['status' => 'active'], ['id' => $id]);
		
		return true;
	}

	public function block($id)
	{
		$this->db->update($this->users, ['status' => 'inactive'], ['id' => $id]);
		
		return true;
	}
}
