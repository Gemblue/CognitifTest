<?php

/**
 * Response
 * 
 * @return mixed
 */
function response($data)
{
    header('Content-Type: application/json');
    echo json_encode($data);
    exit;  
}