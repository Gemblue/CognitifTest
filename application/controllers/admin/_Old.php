<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question extends Backend_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('question_model');
        $this->load->model('course_model');
    }
    
    /**
     * Get question id with ajax mode
     *
     * @return mixed
     */
    public function get_detail($question_id)
    {
        $question = $this->question_model->getDetailQuestion($question_id);
        $options = $this->question_model->getOptionsByQuestion($question_id);
        
        header('Content-Type: application/json');
        echo json_encode(['question' => $question, 'options' => $options]); 
    }

    /**
     * Import question page.
     * 
     * @param int $course_id
     * @param int $category_id
     * @return mixed
     */
    public function import($course_id, $category_id)
    {
        $course = $this->course_model->etDetailCourse($course_id);
        $category = $this->course_model->etDetailCategory($category_id);

        $breadcrumbs = [
            ['link' => url('admin/course'), 'menu' => 'Course'],
            ['link' => url('admin/course/lesson/'. $course_id), 'menu' => 'Back to Lessons']
        ];  

        return view('admin/course/question_import', [
            'category' => $category,
            'course' => $course,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    /**
     * Import question action.
     * 
     * @return mixed
     */
    public function import_action(Request $request)
    {
        $course_id = $this->input->post('course_id');
        $category_id = $this->input->post('category_id');

        // Upload and get file.
        $file = $request->file('question_file');
        $file_name = $file->getClientOriginalName();
        $file_current_name = md5($file_name) . '.csv';
        $request->file('question_file')->move(public_path('upload_temp'), $file_current_name);

        // Define path
        $file_path = public_path('upload_temp') .'/'. $file_current_name;
        
        // Show questions on page preview
        return redirect('admin/course/question/import_preview?file_path=' . $file_path . '&course_id=' . $course_id . '&category_id=' . $category_id);
    }

    /**
     * Preview question
     * 
     * @return mixed
     */
    public function import_preview(Request $request)
    {
        $breadcrumbs = [
            ['link' => url('admin/course'), 'menu' => 'Course'],
            ['link' => url('admin/course/lesson/'. $this->input->post('course_id')), 'menu' => 'Back to Lessons']
        ];

        // Get detail.
        $course = $this->course_model->etDetailCourse($this->input->post('course_id'));
        $category = $this->course_model->etDetailCategory($this->input->post('category_id'));
         
        // Get path.
        $file_path = $this->input->post('file_path');

        // Get question content.
        $questions = Yaml::parse(file_get_contents($file_path));

        // Use markdown
        $Markdown = new \cebe\markdown\GithubMarkdown();

        // Show questions on page preview
        return view('admin/course/question_preview', [
            'category' => $category,
            'course' => $course,
            'questions' => $questions,
            'breadcrumbs' => $breadcrumbs,
            'Markdown' => $Markdown
        ]);
    }

    /**
     * Import do
     * 
     * @return mixed
     */
    public function import_do(Request $request)
    {
    	$course_id = $this->input->post('course_id');
        $category_id = $this->input->post('category_id');
    	$file_path = $this->input->post('file_path');
		
        // Get question content.
        $questions = Yaml::parse(file_get_contents($file_path));

        // Import
        $this->question_model->import($course_id, $category_id, $questions);
        
    	return redirect('admin/course/question/add/' . $course_id . '/' . $category_id);
    }
    
    /**
     * Show add question
     * 
     * @param int $course_id
     * @param int $category_id
     * @return mixed
     */
    public function add($course_id)
    {
        $course = $this->course_model->getDetailCourse($course_id, 'all');
        $questions = $this->question_model->getQuestions($course_id, 'all');
        
        $this->load->view('admin/question/form', [
            'questions' => $questions, 
            'course' => $course,
            'course_id' => $course_id
        ]);
    }

    /**
     * Insert question
     * 
     * @return mixed
     */
    public function insert()
    {
        $course_id = $this->input->post('course_id');
        $hint = $this->input->post('hint');
        $total_option = count($this->input->post('option'));
        $option_final = [];
        
        for ($i=0; $i<$total_option; $i++)
        {
            $option_final[] = [
                'option_content' => $_POST['option'][$i],
                'is_right' => $_POST['status'][$i] 
            ];
        }
        
        $question = [
            'question_content' => $this->input->post('question_content'),
            'creator' => $this->session->userdata('user_id'),
            'hint' => json_encode($hint),
            'question_status' => 'draft'
        ];

        $this->question_model->insertQuestion($course_id, $question, $option_final);
        
        return redirect('admin/question/add/' . $course_id);
    }

    /**
     * Update question
     * 
     * @return mixed
     */
    public function update()
    {
        $course_id = $this->input->post('course_id');
        $hint = $this->input->post('hint');
        $question_id = $this->input->post('question_id');
        $total_option = count($_POST['option']);
        $option_final = [];
        
        for ($i=0; $i<$total_option; $i++)
        {
            $option_final[] = [
                'option_content' => $_POST['option'][$i],
                'is_right' => $_POST['status'][$i], 
                'option_id' => $_POST['option_id'][$i] 
            ];
        }

        $question = [
            'question_content' => $this->input->post('question_content'),
            'hint' => json_encode($hint)
        ];

        $this->question_model->updateQuestion($question_id, $question, $option_final);

        redirect('admin/question/add/'. $course_id);
    }

    /**
     * Remove
     * 
     * @return mixed
     */
    public function remove($question_id)
    {
        $this->question_model->removeQuestion($question_id);
        
        return redirect()->back();   
    }
}