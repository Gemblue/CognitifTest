<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SurveyQuestion extends Backend_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Question_model');
        $this->load->model('Group_model');
        $this->load->library('pagination');
        $this->load->library('form_validation');
    }
    
    /**
     * Show questions list.
     *
     * @return mixed
     */
    public function index($page = 0)
    {
        $config = [
            'base_url' => base_url('admin/SurveyQuestion/index'),
            'total_rows' => $this->Question_model->getTotalQuestions($this->input->get('group_id'), $this->input->get('keyword'), 'all', 'survey'),
            'per_page' => 10
        ];
        
        $this->pagination->initialize($config);

        $questions = $this->Question_model->getQuestions($this->input->get('group_id'), $this->input->get('keyword'), 'all', 'survey', $config['per_page'], $this->uri->segment(4));
        
        $data['questions'] = $questions;
        $data['links'] = $this->pagination->create_links();
        $data['groups'] = $this->Group_model->getGroups('all', null, 'survey');
        
        $this->load->view($this->theme . '/admin/surveyQuestion/list', $data);
    }

    /**
     * Insert.
     *
     * @return mixed
     */
    public function insert()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('question_title', 'Judul Pertanyaan', 'required')
                              ->set_rules('group_id', 'Kelompok Soal', 'required')
                              ->set_rules('question_content', 'Rincian Soal', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            response(['status' => 'failed', 'message' => validation_errors()]);
        }
        
        $this->Question_model->insertQuestion($post['group_id'], $post);
        
        response(['status' => 'success', 'message' => 'Successfully added ..']);
    }

    /**
     * Update.
     *
     * @return mixed
     */
    public function update()
    {
        $post = $this->input->post();
        unset($post['files']);

        $this->form_validation->set_rules('question_id', 'Question ID', 'required')
                              ->set_rules('question_title', 'Judul Pertanyaan', 'required')
                              ->set_rules('question_content', 'Rincian Soal', 'required');
        
        if ($this->form_validation->run() == FALSE) 
        {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">'. validation_errors() .'</div>');
            redirect($_SERVER['HTTP_REFERER']);
        }
        
        $this->Question_model->updateQuestion($post);
        
        $this->session->set_flashdata('message', '<div class="alert alert-success">Successfully updated ..</div>');
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Show edit form
     *
     * @return mixed
     */
    public function edit($id)
    {
        $data['question'] = $this->Question_model->getDetailQuestion($id);
        $data['groups'] = $this->Group_model->getGroups('all', null, 'quiz');

        $this->load->view($this->theme . '/admin/surveyQuestion/form', $data);
    }

    /**
     * Publish
     *
     * @return mixed
     */
    public function publish($id)
    {
        $this->course_model->publish($id);
        
        $this->session->set_flashdata('message', $this->lang->line('jooglo_success_global'));
		redirect('admin/master');
    }

    /**
     * Draft
     *
     * @return mixed
     */
    public function draft($id)
    {
        $this->course_model->draft($id);
        
        $this->session->set_flashdata('message', $this->lang->line('jooglo_success_global'));
		redirect('admin/master');
    }
}