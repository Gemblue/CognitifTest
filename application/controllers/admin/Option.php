<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Option extends Backend_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Option_model');
        $this->load->library('pagination');
        $this->load->library('form_validation');
    }
    
    /**
     * Show questions list.
     *
     * @return mixed
     */
    public function index($page = 0)
    {
        $config = [
            'base_url' => base_url('admin/question/index'),
            'total_rows' => $this->Option_model->getTotalQuestions(null, 'all'),
            'per_page' => 10
        ];

        $this->pagination->initialize($config);

        $data['questions'] = $this->Option_model->getQuestions(null, null, 'all', 'quiz', $config['per_page'], $this->uri->segment(4));
        $data['links'] = $this->pagination->create_links();
        $data['groups'] = $this->Option_model->getGroups('all', null, 'quiz');
        
        $this->load->view($this->theme . '/admin/question/list', $data);
    }

    /**
     * Detail.
     *
     * @return mixed
     */
    public function detail($id)
    {
        response($this->Option_model->getDetail($id));
    }

    /**
     * Insert.
     *
     * @return mixed
     */
    public function insert()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('option_content', 'Jawaban Opsi', 'required')
                              ->set_rules('is_right', 'Status Jawaban', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            response(['status' => 'failed', 'message' => validation_errors()]);
        }
        
        $this->Option_model->insert($post);
        
        response(['status' => 'success', 'message' => 'Successfully added ..']);
    }

    /**
     * Update.
     *
     * @return mixed
     */
    public function update()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('option_content', 'Jawaban Opsi', 'required')
                              ->set_rules('is_right', 'Status Jawaban', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            response(['status' => 'failed', 'message' => validation_errors()]);
        }

        $this->Option_model->update($post);

        response(['status' => 'success', 'message' => 'Successfully updated ..']);
    }
}