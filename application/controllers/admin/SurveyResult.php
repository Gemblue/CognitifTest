<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SurveyResult extends Backend_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Report_model');
        $this->load->model('Question_model');
        $this->load->model('Hint_model');
        $this->load->model('School_model');
        $this->load->model('Group_model');
        
        $this->load->library('pagination');
    }
    
    /**
     * Survey Result homepage.
     * 
     * @return mixed
     */
    public function index($page = 0)
    {
        $filters = [
            'name' => $this->input->get('name'),
            'nisn' => $this->input->get('nisn'),
            'school_id' => $this->input->get('school_id'),
            'group_id' => $this->input->get('group_id'),
            'label' => 'survey'
        ];

        $config = [
            'base_url' => base_url('admin/surveyResult/index/' . $page),
            'total_rows' => $this->Report_model->getTotalResults($filters),
            'per_page' => 10
        ];

        $this->pagination->initialize($config);

        $data['results'] = $this->Report_model->getResults($filters, $config['per_page'], $this->uri->segment(3));
        $data['schools'] = $this->School_model->getSchools('active');
        $data['groups'] = $this->Group_model->getGroups('all', null, 'survey');
        $data['links'] = $this->pagination->create_links();
        
        $this->load->view($this->theme . '/admin/surveyResult/list', $data);
    }

    /**
     * Export Report.
     *
     * @return mixed
     */
    public function export()
    {
        $filters = [
            'name' => $this->input->get('name'),
            'nisn' => $this->input->get('nisn'),
            'school_id' => $this->input->get('school_id'),
            'group_id' => $this->input->get('group_id')
        ];

        $results = $this->Report_model->getResults($filters);
        
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H:i:s').'#report.csv"');
        header('Pragma: no-cache');
        header('Expires: 0');
        
        $file = fopen('php://output', 'w');
        
        // Create data title
        fputcsv($file, ['Kelompok Soal', 'Nama Siswa', 'Sekolah', 'NISN', 'Skor', 'Tanggal Uji']);

        foreach($results as $result) 
        {
            fputcsv($file, [
                $result->title,
                $result->name, 
                $result->school_name, 
                $result->nisn, 
                $result->score, 
                $result->created_at
            ]);
        }        
    }
    
    /**
     * Report detail.
     *
     * @return mixed
     */
    public function detail($id = null)
    {
        $data['result'] = $this->Report_model->getResult($id);
        $data['answer_logs'] = $this->Question_model->getAnswerLogs($id);
        
        $this->load->view($this->theme . '/admin/surveyResult/detail', $data);
    }
}