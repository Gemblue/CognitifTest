<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Backend_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Stats_model');
        $this->load->model('Report_model');
        $this->load->model('Question_model');
    }

    /**
     * Homepage admin.
     *
     * @return mixed
     */
    public function index()
    {
        // Total
        $data['total_users'] = $this->Stats_model->getTotalUsers();
        $data['total_group'] = $this->Stats_model->getTotalGroup();
        $data['total_questions'] = $this->Stats_model->getTotalQuestions();
        $data['total_school'] = $this->Stats_model->getTotalSchool();
        $data['results'] = $this->Report_model->getResults(null, 6, 0);
        $data['questions'] = $this->Question_model->getQuestions(null, null, 'all', 'quiz', 6, 0);
        
        $this->load->view($this->theme . '/admin/dashboard', $data);   
    }
}