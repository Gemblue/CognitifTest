<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Survey extends Backend_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Group_model');
        $this->load->model('Question_model');
        $this->load->library('pagination');
        $this->load->library('form_validation');
    }
    
    /**
     * Show list.
     *
     * @return mixed
     */
    public function index()
    {
        $config = [
            'base_url' => base_url('admin/group'),
            'total_rows' => $this->Group_model->getTotalGroups('all', $this->input->get('keyword'), 'survey'),
            'per_page' => 10,
            'page_query_string' => true,
            'query_string_segment' => 'page'
        ];

        $this->pagination->initialize($config);
        
        $data['groups'] = $this->Group_model->getGroups('all', $this->input->get('keyword'), 'survey', $config['per_page'], $this->input->get('page'));
        $data['links'] = $this->pagination->create_links();
        
        $this->load->view($this->theme . '/admin/survey/list', $data);
    }

    /**
     * Detail.
     *
     * @return mixed
     */
    public function detail($id)
    {
        response($this->Group_model->getDetail($id));
    }

    /**
     * Insert.
     *
     * @return mixed
     */
    public function insert()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('title', 'Judul Kelompok Soal', 'required')
                              ->set_rules('subtitle', 'Sub-Judul', 'required')
                              ->set_rules('status', 'Status', 'required')
                              ->set_rules('duration', 'Durasi', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            response(['status' => 'failed', 'message' => validation_errors()]);
        }

        $this->Group_model->insert($post);

        response(['status' => 'success', 'message' => 'Successfully added ..']);
    }

    /**
     * Update.
     *
     * @return mixed
     */
    public function update()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('title', 'Judul Kelompok Soal', 'required')
                              ->set_rules('subtitle', 'Sub-Judul', 'required')
                              ->set_rules('status', 'Status', 'required')
                              ->set_rules('duration', 'Durasi', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            response(['status' => 'failed', 'message' => validation_errors()]);
        }

        $this->Group_model->update($post);
        
        response(['status' => 'success', 'message' => 'Successfully updated ..']);
    }
}