<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends Backend_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('User_model');
        $this->load->model('School_model');
        $this->load->library('pagination');
        $this->load->library('form_validation');
    }
    
    /**
     * Show student list.
     *
     * @return mixed
     */ 
    public function index()
    {
        $config = [
            'base_url' => base_url('admin/student'),
            'total_rows' => $this->User_model->get_all_users('total', $this->input->get('keyword'), 'all'),
            'per_page' => 10,
            'page_query_string' => true,
            'query_string_segment' => 'page'
        ];

        $this->pagination->initialize($config);

        $data['students'] = $this->User_model->get_all_users('data', $this->input->get('keyword'), 'all', $config['per_page'], $this->input->get('page'));
        $data['schools'] = $this->School_model->getSchools('active');
        $data['links'] = $this->pagination->create_links();
        
        $this->load->view($this->theme . '/admin/student/list', $data);
    }

    /**
     * Insert.
     *
     * @return mixed
     */
    public function insert()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('name', 'Nama', 'required')
                              ->set_rules('nisn', 'NISN', 'required')
                              ->set_rules('username', 'Nama Pengguna', 'required')
                              ->set_rules('email', 'Email', 'required')
                              ->set_rules('school_id', 'Sekolah', 'required')
                              ->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            response(['status' => 'failed', 'message' => validation_errors()]);
        }

        $post['role_id'] = 7;

        $this->User_model->insert($post);

        response(['status' => 'success', 'message' => 'Successfully added ..']);
    }

    /**
     * Update.
     *
     * @return mixed
     */
    public function update()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('name', 'Nama', 'required')
                              ->set_rules('nisn', 'NISN', 'required')
                              ->set_rules('school_id', 'Sekolah', 'required')
                              ->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            response(['status' => 'failed', 'message' => validation_errors()]);
        }

        if (empty($post['password']))
            unset($post['password']);

        $this->User_model->update($post['id'], $post);
        
        response(['status' => 'success', 'message' => 'Successfully updated ..']);
    }

    /**
     * Detail.
     *
     * @return mixed
     */
    public function detail($id)
    {
        response($this->User_model->get_detail($id));
    }

    /**
     * Remove.
     *
     * @return mixed
     */
    public function remove($id)
    {
        $this->User_model->delete($id);
        
        $this->session->set_flashdata('message', '<div class="alert alert-danger">Berhasil dihapus ..</div>');
        
        redirect($_SERVER['HTTP_REFERER']);
    }
}