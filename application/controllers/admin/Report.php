<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Backend_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Report_model');
        $this->load->model('Question_model');
        $this->load->model('Hint_model');
        $this->load->model('School_model');
        $this->load->model('Group_model');
        
        $this->load->library('pagination');
    }
    
    /**
     * Report homepage.
     *
     * @return mixed
     */
    public function index($page = 0)
    {
        $filters = [
            'name' => $this->input->get('name'),
            'nisn' => $this->input->get('nisn'),
            'school_id' => $this->input->get('school_id'),
            'group_id' => $this->input->get('group_id'),
            'label' => 'quiz'
        ];

        $config = [
            'base_url' => base_url('admin/report/index/' . $page),
            'total_rows' => $this->Report_model->getTotalResults($filters),
            'per_page' => 10
        ];

        $this->pagination->initialize($config);

        $data['results'] = $this->Report_model->getResults($filters, $config['per_page'], $this->uri->segment(3));
        $data['schools'] = $this->School_model->getSchools('active');
        $data['groups'] = $this->Group_model->getGroups('all', null, 'quiz');
        $data['links'] = $this->pagination->create_links();
        
        $this->load->view($this->theme . '/admin/report/list', $data);
    }

    /**
     * Export Report.
     *
     * @return mixed
     */
    public function export()
    {
        $filters = [
            'name' => $this->input->get('name'),
            'nisn' => $this->input->get('nisn'),
            'school_id' => $this->input->get('school_id'),
            'group_id' => $this->input->get('group_id'),
            'label' => 'quiz'
        ];

        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H:i:s').'#report.csv"');
        header('Pragma: no-cache');
        header('Expires: 0');
        
        $file = fopen('php://output', 'w');
        
        $title = ['No', 'Nama Siswa', 'Sekolah', 'Kelompok Soal'];
        
        for ($i=1;$i<=40;$i++)
        {
            array_push($title, 'Soal ' . $i);
            array_push($title, 'Akses Hint');
            array_push($title, 'Hint Prosedural ' . $i);
            array_push($title, 'Hint Strategi ' . $i);
            array_push($title, 'Hint Deklaratif ' . $i);
        }

        fputcsv($file, $title);
        
        $results = $this->Report_model->getResultsWithHint($filters);
        
        $num = 1;
        $content = [];
        
        foreach ($results as $result)
        {
            $content = [
                $num,
                $result['nama_siswa'],
                $result['sekolah'],
                $result['kelompok_soal']
            ];

            foreach($result['log']  as $l)
            {
                array_push($content, $l['soal']);
                array_push($content, $l['akses_hint']);
                array_push($content, $l['hint_prosedural']);
                array_push($content, $l['hint_deklaratif']);
                array_push($content, $l['hint_strategi']);
            }
            
            $num++;

            fputcsv($file, $content);
        }
    }
    
    /**
     * Report detail.
     *
     * @return mixed
     */
    public function detail($id = null)
    {
        $data['result'] = $this->Report_model->getResult($id);
        $data['answer_logs'] = $this->Question_model->getAnswerLogs($id);
        $data['total_hint_duration'] = $this->Hint_model->getTotalDurationHintLog($id);
        
        $this->load->view($this->theme . '/admin/report/detail', $data);
    }
}