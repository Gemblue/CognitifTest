<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hint extends Backend_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Hint_model');
        $this->load->library('pagination');
        $this->load->library('form_validation');
    }
    
    /**
     * Detail.
     *
     * @return mixed
     */
    public function detail($id)
    {
        response($this->Hint_model->getDetail($id));
    }

    /**
     * Logs.
     * 
     * @return mixed
     */
    public function logs($session_id, $question_id)
    {
        response($this->Hint_model->getHintLogs($session_id, $question_id));
    }

    /**
     * Insert.
     *
     * @return mixed
     */
    public function insert()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('hint_content', 'Petunjuk/Konten', 'required')
                              ->set_rules('hint_category', 'Kategori', 'required')
                              ->set_rules('hint_sort', 'Sort', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            response(['status' => 'failed', 'message' => validation_errors()]);
        }
        
        $this->Hint_model->insert($post);
        
        response(['status' => 'success', 'message' => 'Successfully added ..']);
    }

    /**
     * Update.
     *
     * @return mixed
     */
    public function update()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('hint_content', 'Petunjuk/Konten', 'required')
                              ->set_rules('hint_category', 'Kategori', 'required')
                              ->set_rules('hint_sort', 'Sort', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            response(['status' => 'failed', 'message' => validation_errors()]);
        }

        $this->Hint_model->update($post);

        response(['status' => 'success', 'message' => 'Successfully updated ..']);
    }
}