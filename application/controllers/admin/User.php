<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends Backend_Controller 
{
	public function __construct()
	{
		parent::__construct();

        $this->load->library('pagination');
        $this->load->library('form_validation');
        $this->load->model('User_model');
        $this->load->helper('time_helper');
        $this->lang->load('jooglo', 'english');
	}

	public function index($status = 'all')
	{
		$data['title_page'] = 'User';
		$data['total'] = $this->User_model->get_all_users('total', $status);
		
		$config['base_url'] = site_url('admin/user/' . $status);
		$config['total_rows'] = $data['total'];
		$config['per_page'] = 10;
		$config['uri_segment'] = 4;
		
		$this->pagination->initialize($config);
		
		$data['pagination'] = $this->pagination->create_links();
		$data['results'] = $this->User_model->get_all_users('data', $status, $config['per_page'], $this->uri->segment(5));
		
		$this->load->view('admin/user/user', $data);
	}

	public function edit($user_id)
	{
		$data['title_page'] = 'Edit User';
		$data['form_type'] = 'edit';
		$data['result'] = $this->User_model->get_detail($user_id);
		$data['avatar'] = $this->User_model->get_avatar($user_id);
		$data['roles'] = $this->User_model->get_all_roles();
        
		$this->load->view('admin/user/form', $data);
	}

	public function update()
	{
        $param = $_POST;

		if ($this->User_model->is_exist_username($param['username']))
		{
			$current_username = $this->User_model->get_username($param['user_id'], 'id');

			if ($param['username'] != $current_username)
			{
				$this->session->set_flashdata('message', $this->lang->line('jooglo_error_exist'));

				redirect('admin/user/edit/' . $param['user_id']);
			}
		}

		$this->User_model->update($param['user_id'], [
            'name' => $param['name'],
            'nisn' => $param['nisn'],
            'role_id' => $param['role_id'],
            'handphone' => $param['handphone'],
            'jenis_kelamin' => $param['jenis_kelamin'],
			'username' => $param['username'],
			'email' => $param['email']
        ]);
        
        // Update password
        if (isset($_POST['password']))
        {
            $this->User_model->update($param['user_id'], [
                'password' => md5($param['password'])
            ]);
        }
		
		$this->session->set_flashdata('message', $this->lang->line('jooglo_success_update'));
		
		redirect('admin/user/edit/' . $param['user_id']);
	}

	public function activate($id)
	{
		$this->User_model->activate($id);
		
		$this->session->set_flashdata('message', $this->lang->line('jooglo_success_global'));
		
		redirect('admin/user');
	}

	public function block($id)
	{
		$this->User_model->block($id);
		 
		$this->session->set_flashdata('message', $this->lang->line('jooglo_success_global'));
		 
		redirect('admin/user');
	}

	public function search($status = null)
	{
		$data['keyword'] = $this->input->post('keyword');
		$data['title_page'] = 'User';
		$data['total'] = $this->User_model->search_users('total', $status, $data['keyword']);
		
		$config['base_url'] = site_url('admin/user/' . $status);
		$config['total_rows'] = $data['total'];
		$config['per_page'] = 10;
		$config['uri_segment'] = 5;
		
		$this->pagination->initialize($config);
		
		$data['pagination'] = $this->pagination->create_links();
		$data['results'] = $this->User_model->search_users('data', $status, $data['keyword'], $config['per_page'], $this->uri->segment(5));
		
		$this->load->view('admin/user/user', $data);
	}

	public function add()
	{
	 	$data['title_page'] = 'New User';
		$data['form_type'] = 'new';
		$data['roles'] = $this->User_model->get_all_roles();
        
		$this->load->view('admin/user/form', $data);
	}

	public function insert()
	{
        $param = $_POST;
        $param['status'] = 'active';
        
		// Validation
		$this->form_validation->set_rules('role_id', 'Role', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('handphone', 'Handphone', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('nisn', 'NISN', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');

		if ($this->form_validation->run() == false)
		{
			$this->session->set_flashdata('message', '<div class="alert alert-danger">' . validation_errors() . '</div>');
			redirect('admin/user/add');
			exit;
		}

		// Check username
		if ($this->User_model->is_exist_username($param['username']))
		{
			$this->session->set_flashdata('message', $this->lang->line('jooglo_error_exist_username'));
			redirect('admin/user/add');
			exit;
		}

		// Check email
		if ($this->User_model->is_exist_email($param['email']))
		{
			$this->session->set_flashdata('message', $this->lang->line('jooglo_error_exist_email'));
			redirect('admin/user/add');
			exit;
		}

		// Insert new user
		$user_id = $this->User_model->insert($param);

		$this->session->set_flashdata('message', $this->lang->line('jooglo_success_add'));
		redirect('admin/user/edit/' . $user_id);
	}

	public function delete($id)
	{
		$this->User_model->delete($id);
		
		$this->session->set_flashdata('message', $this->lang->line('jooglo_success_delete'));
		
		redirect('admin/user');
	}
}
