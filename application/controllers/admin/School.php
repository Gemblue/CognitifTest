<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School extends Backend_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('School_model');
        $this->load->library('pagination');
        $this->load->library('form_validation');
    }
    
    /**
     * Show list.
     *
     * @return mixed
     */
    public function index()
    {
        $config = [
            'base_url' => base_url('admin/school'),
            'total_rows' => $this->School_model->getTotalSchools('all', $this->input->get('keyword')),
            'per_page' => 10,
            'page_query_string' => true,
            'query_string_segment' => 'page'
        ];
        
        $this->pagination->initialize($config);

        $data['schools'] = $this->School_model->getSchools('all', $this->input->get('keyword'), $config['per_page'], $this->input->get('page'));
        $data['links'] = $this->pagination->create_links();
        
        $this->load->view($this->theme . '/admin/school/list', $data);
    }

    /**
     * Detail.
     *
     * @return mixed
     */
    public function detail($id)
    {
        response($this->School_model->getDetail($id));
    }

    /**
     * Insert.
     *
     * @return mixed
     */
    public function insert()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('name', 'Nama Sekolah', 'required')
                              ->set_rules('npsn', 'NPSN', 'required');
                
        if ($this->form_validation->run() == FALSE) {
            response(['status' => 'failed', 'message' => validation_errors()]);
        }

        $this->School_model->insert($post);

        response(['status' => 'success', 'message' => 'Successfully added ..']);
    }

    /**
     * Update.
     *
     * @return mixed
     */
    public function update()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('name', 'Nama Sekolah', 'required')
                              ->set_rules('npsn', 'NPSN', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            response(['status' => 'failed', 'message' => validation_errors()]);
        }

        $this->School_model->update($post);

        response(['status' => 'success', 'message' => 'Successfully updated ..']);
    }
}