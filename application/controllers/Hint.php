<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hint extends Frontend_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Hint_model');
    }
    
    /**
     * Detail.
     *
     * @return mixed
     */
    public function detail($hint_id, $question_id)
    {
        // Save log.
        $hint_log_id = $this->Hint_model->saveLog([
            'result_id' => $this->session->session_id,
            'question_id' => $question_id,
            'hint_id' => $hint_id
        ]);
        
        response(array_merge(['hint_log_id' => $hint_log_id], $this->Hint_model->getDetail($hint_id)));
    }

    /**
     * Close Hint.
     *
     * @return mixed
     */
    public function close()
    {
        $hint_log_id = $this->input->post('hint_log_id');
        $duration = $this->input->post('duration');

        $this->Hint_model->updateLog($hint_log_id, $duration);
        
        response(['status' => 'Updated!']);
    }
}