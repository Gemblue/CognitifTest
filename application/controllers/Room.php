<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Room extends Frontend_Controller
{
	public function __construct()
	{
        parent::__construct();
        
        $this->load->library('pagination');
    }
    
	public function index()
	{
        $data['groups'] = $this->Group_model->getGroupsBySchool('publish', null, 'quiz', $this->session->userdata('school_id'));
        $data['surveys'] = $this->Group_model->getGroups('publish', null, 'survey');
        
        $this->load->view($this->theme . '/home', $data);
    }

    public function session($group_id)
	{
        // Cek group
        $group = $this->Group_model->getDetail($group_id);

        if (empty($group)) 
        {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">Kelompok soal tidak diketahui ..</div>');
            redirect('room');
        }
        
        // Cek terlebih hadulu kelompok tersebut punya pertanyaan apa tidak
        $total = $this->Question_model->getTotalQuestions($group_id, null, 'publish');

        if ($total <= 0) 
        {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">Maaf, kelompok soal tersebut belum siap untuk digunakan.</div>');
            redirect('room');
        }

        // Making session .. 
        $session_id = $this->Question_model->generateSession($group_id, $this->session->userdata('user_id'));

        $this->session->set_userdata('label', $group['label']);
        $this->session->set_userdata('session_id', $session_id);
        $this->session->set_userdata('group_id', $group_id);
        $this->session->set_userdata('start_time', date('Y-m-d H:i:s'));

        redirect('room/exercise');
    }

    public function exercise()
	{
        $data['questions'] = $this->Question_model->getQuestions($this->session->group_id, null, 'all', $this->session->userdata('label'));
        $data['group'] = $this->Group_model->getDetail($this->session->group_id);
        $data['total'] = count($data['questions']);
        
        $this->load->view($this->theme . '/' . $this->session->userdata('label'), $data);
    }

    public function finish()
	{
        $posts = $this->input->post();

        // Save answer log.
        foreach ($posts as $key => $value)
        {
            $question = explode('_', $key);
            
            $this->Question_model->answer($this->session->session_id, $question[0], $value);
        }
        
        // Save result.
        $start_time = strtotime($this->session->start_time);
        $end_time = time();
        $duration = $end_time - $start_time;
        
        // Save final result!
        $save = $this->Question_model->saveResult($this->session->session_id, $this->session->group_id, $duration);
        
        if ($this->session->userdata('label') == 'survey')
        {
            $this->session->set_flashdata('message' , '<div class="alert alert-success">Terimakasih, jawaban kamu sudah terkirim!</div>');
            redirect('room');
        }
        else
        {
            $this->session->set_userdata('score', $save['score']);
            
            $data['group'] = $this->Group_model->getDetail($this->session->group_id);
            
            $this->load->view($this->theme . '/finish', $data);
        }
    }
}