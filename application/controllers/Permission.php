<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permission extends MY_Controller
{
	public function __construct()
	{
        parent::__construct();
        
        $this->load->library('ci_auth');
        $this->load->library('form_validation');
        $this->load->model('School_model');
    }

	public function index()
	{
        $data['schools'] = $this->School_model->getSchools('active');

        $this->load->view($this->theme . '/login', $data);
    }

    public function confirm($token)
	{
        $confirm = $this->ci_auth->confirm($token);
        
        $this->session->set_flashdata('message', '<div class="alert alert-info">'. $confirm['message'] .'</div>');
		redirect('permission');
	}

    public function login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        
        $login = $this->ci_auth->login($email, $password);

        if ($login['status'] == 'failed')
        {
            $this->session->set_flashdata('message', $login['message']);

            redirect('permission');
        }

        if ($login['role_name'] == 'Super')
        {
            redirect('admin/dashboard');
            exit;
        }

        redirect('room');
    }

    public function register()
    {
        $data['schools'] = $this->School_model->getSchools('active');

        $this->load->view($this->theme . '/register', $data);
    }

    public function logout()
    {
        $this->ci_auth->logout();
        
        redirect('permission');
    }

    public function register_action()
    {
        $param = $_POST;
        
        $this->session->set_flashdata($_POST);

        // Validation
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('nisn', 'NISN', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('password_confirm', 'Password Confirm', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('school_id', 'School', 'required');

		if ($this->form_validation->run() == false)
		{
			$this->session->set_flashdata('message', '<div class="alert alert-danger">' . validation_errors() . '</div>');
			redirect('permission/register');
			exit;
        }

        $register = $this->ci_auth->register($param, 'active', 7, false);
        
        if ($register['status'] == 'failed')
        {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">' . $register['message'] . '</div>');
			redirect('permission/register');
			exit;
        }
        
        $this->session->set_flashdata('message', '<div class="alert alert-success">' . $register['message'] . '</div>');
        redirect('permission');
    }
}