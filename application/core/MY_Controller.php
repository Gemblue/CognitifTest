<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public $theme;

	// Bootstrap!
	public function __construct()
	{
		parent::__construct();

		date_default_timezone_set('Asia/Jakarta');
        
        $this->load->library('ci_auth');
        $this->load->helper('rest');
        $this->load->helper('time');
        
    	// Profiler
        $this->output->enable_profiler(false);
        
        // Set theme.
        $this->theme = 'simple';
	}
}

class Frontend_Controller extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
        
        $this->load->model('Question_model');
        $this->load->model('Group_model');
        
		// This page is only logged in user.
        if (!$this->ci_auth->logged_in())
        {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">Login required.</div>');
            redirect('permission');
        }
	}
}

class Backend_Controller extends MY_Controller
{
    public function __construct()
	{
        parent::__construct();
        
        // This page is only for admin.
        if (!$this->ci_auth->logged_in())
        {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">Only for super admin.</div>');
            redirect('permission');
        }

        $role_name = $this->session->userdata('role_name');

        if ($role_name != 'Super')
        {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">Only for super admin.</div>');
            redirect('permission');
        }
	}
}